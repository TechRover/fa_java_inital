package com.trs.fa.lib.cloudstorage;

import com.google.auth.Credentials;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.Bucket;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import com.trs.fa.common.model.AdminConfiguration;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;


public class GoogleStorageManager {

    AdminConfiguration adminConfiguration;

    public GoogleStorageManager(AdminConfiguration adminConfiguration) {
        this.adminConfiguration = adminConfiguration;
    }

    public byte[] getFile(String key) throws IOException {
        return getBucket().get(key).getContent();
    }

    public void deleteFile(String key) throws IOException {
        BlobId blobId = BlobId.of(adminConfiguration.getBucketName(),key);
        getStorageConnection().delete(blobId);
    }


    public String uploadFile(MultipartFile file) throws IOException {
        return uploadFile(file.getName(),file.getBytes(),file.getContentType());
    }

    public String uploadFile(MultipartFile file,String name) throws IOException {
        // TODO Add The Logic To Update the file
        return uploadFile(name,file.getBytes(),file.getContentType());
    }

    public String uploadFile(String name,byte[] data,String contentType) throws IOException {
        getBucket().create(name,data,contentType);
        return name;
    }



    public Bucket getBucket() throws IOException {
        Storage storage = getStorageConnection();
        return storage.get(adminConfiguration.getBucketName());
    }


    public Storage getStorageConnection() throws IOException {

        Credentials credentials = GoogleCredentials
                .fromStream(new ByteArrayInputStream(adminConfiguration.getGoogleCloudConfig().getBytes(StandardCharsets.UTF_8)));
        return StorageOptions.newBuilder().setCredentials(credentials)
                .setProjectId(adminConfiguration.getGoogleCloudProjectId()).build().getService();
    }


    public InputStream getFile(){
        InputStream is = getClass().getClassLoader().getResourceAsStream("auth/fastening.json");
        return is;
    }
}
