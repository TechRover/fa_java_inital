package com.trs.fa;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@Slf4j
public class FACoreApplication {

	public static void main(String[] args) {
		log.info("LOADING-");
		SpringApplication.run(FACoreApplication.class, args);
	}

}
