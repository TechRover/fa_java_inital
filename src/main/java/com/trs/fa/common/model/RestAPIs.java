package com.trs.fa.common.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document("rest_apis")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class RestAPIs extends PathTrail {

	@Id
	String id;
	
	String name;

	String roleGroup;

	String apiGroup;
	
	String description;

	List<String> roles;
	
}
