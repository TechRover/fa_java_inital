package com.trs.fa.common.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;

@Data
@Document("translations")
public class Translations {

    String name;

    String lang;


    Map<String,String> translation;

    public Translations() {
        translation = new HashMap<>();
    }
}
