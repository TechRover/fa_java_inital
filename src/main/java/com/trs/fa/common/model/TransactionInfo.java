package com.trs.fa.common.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TransactionInfo {

    String transactionId;

    String transactionFeeCurrency;

    String transactionFeeValue;
}
