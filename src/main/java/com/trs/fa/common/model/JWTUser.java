package com.trs.fa.common.model;


import io.jsonwebtoken.Claims;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JWTUser {
    private enum ClaimFiledNames{
        ID,SESSION_ID,ROLE
    }

    String id;
    String sessionId;
    List<String> role;

    public Map<String, Object> toClaim(){
        Map<String, Object> claim   = new HashMap<>();
        claim.put(ClaimFiledNames.ID.toString(),id);
        claim.put(ClaimFiledNames.SESSION_ID.toString(),sessionId);
        claim.put(ClaimFiledNames.ROLE.toString(),role);
        return claim;
    }

    public boolean hasRole(String role){
        return this.role.contains(role);
    }

    public static JWTUser fromClaim(Claims claim){
        JWTUser jwtUser = new JWTUser();
        jwtUser.setId((String) claim.get(ClaimFiledNames.ID.toString()));
        jwtUser.setSessionId((String) claim.get(ClaimFiledNames.SESSION_ID.toString()));
        jwtUser.setRole((List<String>) claim.get(ClaimFiledNames.ROLE.toString()));
        return jwtUser;
    }
}
