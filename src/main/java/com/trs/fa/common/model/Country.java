package com.trs.fa.common.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.trs.fa.common.utils.SoftDeleteModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document("country")
@Data
@NoArgsConstructor
public class Country extends PathTrail implements SoftDeleteModel {

    @Id
    String id;

    String name;

    String countryCode;

    String shortCode;

    String shortCode2;

    @JsonIgnore
    boolean softDelete;
}
