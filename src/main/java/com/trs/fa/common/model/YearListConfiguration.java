package com.trs.fa.common.model;


import lombok.Data;

@Data
public class YearListConfiguration {

    int startYear = 2020;

    int endYear;


    public YearListConfiguration() {
        endYear = 2030;
    }
}
