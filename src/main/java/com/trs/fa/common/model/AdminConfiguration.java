package com.trs.fa.common.model;

import com.trs.fa.common.enums.StorageServiceType;
import com.trs.fa.common.utils.FileReader;
import com.trs.fa.common.utils.Role;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;

@Document("admin_config")
@AllArgsConstructor
@Data
@Builder
@ToString
public class AdminConfiguration extends PathTrail {



	String contactUsEmailTemplate;


	int defaultPageSize;

	String mailSubject;

	String fromEmail;

	String mailHost;
	String mailPort;
	String mailUserName;
	String mailPassword;
	Boolean mailSmtpAuth;
	Boolean mailStarttls;

	String fileUploadAccessKey;
	String fileUploadSecretKey;
	String bucketName;

	String region;
	String endPoint;
	String fileUrlTemplate;

	double imageMaxSize;
	double imageThumbnailGenerateScale;


	// AWS Server Configuration
	String smsAwsAccessKey;
	String smsAwsSecretKey;
	String smsAwsRegion;



	String firebaseServerKey;

	String baseUrl;

	String currency;
	String currencyCode;
	String countryCode;

	//Affiny pay payment configuration
	String affinyPayAccountId;
	String affinyPayAccessToken;
	String affinyPayMerchantId;

	double taxInPercent;

	String termsAndCondition;


	PushNotificationConfig notificationSettings;

	Map<Role,Integer> maxSessions;


	YearListConfiguration yearListConfiguration = new YearListConfiguration();


	int referenceCodeLength;


	StorageServiceType storageServiceType;


	String googleCloudConfig;

	String googleCloudProjectId;
	String googleCloudBucketName;

	public AdminConfiguration() {


		referenceCodeLength = 10;

		maxSessions = new HashMap<>();
		maxSessions.put(Role.ADMIN,0);
		maxSessions.put(Role.USER,2);

		countryCode = "+1";


		/// Defaults
		storageServiceType = StorageServiceType.CLOUD_STORAGE;

		googleCloudConfig = FileReader.loadFile("auth/fastening.json");

		googleCloudProjectId = "freeagency-dev";

		googleCloudBucketName = "freeagency_dev";

		region = "us-west-2";
		endPoint = "s3-us-west-2.amazonaws.com";
		fileUrlTemplate = "https://{{endPoint}}/{{bucketName}}/{{fileUrl}}";

		imageMaxSize = 1;
		imageThumbnailGenerateScale = 50;



		notificationSettings = new PushNotificationConfig();

	}

}
