package com.trs.fa.common.model;

import lombok.Data;

@Data
public class PushNotificationSetting {


    String dateFormat;

    String timeFormat;

    public PushNotificationSetting(String dateFormat, String timeFormat) {
        this.dateFormat = dateFormat;
        this.timeFormat = timeFormat;
    }
}
