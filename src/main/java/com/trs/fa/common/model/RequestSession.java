package com.trs.fa.common.model;


import lombok.*;

import java.util.Locale;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class RequestSession {

    JWTUser jwtUser;

    String timezone;

    Locale locale = Locale.ENGLISH;
}
