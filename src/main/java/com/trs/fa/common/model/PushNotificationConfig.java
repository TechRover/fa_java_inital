package com.trs.fa.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PushNotificationConfig {

    // Appointment Notifications ...
    PushMessage appointmentCreateNotification;
    PushMessage appointmentCanceled;
    PushMessage appointmentConfirmed;
    PushMessage appointmentCheckedIn;

    // Medication Notifications ...
    PushMessage medicationSharedNotification;

    // Wellness Notifications ...
    PushMessage wellnessSharedNotification;

    // Document Notifications ...
    PushMessage shareNotification;

    PushNotificationSetting setting;


    public PushNotificationConfig() {
        // Appointment Notifications Defaults  ...
        appointmentCreateNotification = new PushMessage("New Appointment Added","{{firstName}}, has booked an appointment on {{day}} {{startTime}} - {{endTime}}");
        appointmentCanceled = new PushMessage("Appointment Canceled","Appointment has been canceled for {{day}} {{startTime}} - {{endTime}}");
        appointmentConfirmed = new PushMessage("Appointment Confirmed","Appointment has been confirmed by office on {{day}} {{startTime}} - {{endTime}}");
        appointmentCheckedIn = new PushMessage("Appointment Confirmed","Patient has been checked in for appointment for  {{day}} {{startTime}} - {{endTime}}");

        // Medications Notifications Defaults  ...
        medicationSharedNotification = new PushMessage("Patient has shared medications","{{firstName}} has shared the medication with you");

        // Wellness Notifications Defaults...
        wellnessSharedNotification = new PushMessage("Patient has shared wellness journal","{{firstName}} has shared the wellness journal with you");

        // Document Notifications Defaults...
        shareNotification = new PushMessage("Patient has shared his {{type}}","{{sharedBy.firstName}} has shared the {{type}} with you");

        setting = new PushNotificationSetting("MM/dd/yyyy","hh:mm a");
    }


}
