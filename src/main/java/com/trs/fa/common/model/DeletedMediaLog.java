package com.trs.fa.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document("deleted_media_log")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DeletedMediaLog extends PathTrail {

    @Id
    String id;

    String link;

    String thumbnailLink;

    String userId;

    Date date;
}
