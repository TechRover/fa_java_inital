package com.trs.fa.common.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PushMessage {

    String title;

    String body;

    public PushMessage(String title, String body) {
        this.title = title;
        this.body = body;
    }
}
