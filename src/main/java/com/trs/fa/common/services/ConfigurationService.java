package com.trs.fa.common.services;

import com.trs.fa.common.model.AdminConfiguration;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * Event Configuration Service to set and update event related configuration from admin persona
 * @author TechRover Solutions.
 *
 */
public interface ConfigurationService {


	/**
	 * Retrieve Community Configuration.
	 * Single record in mongo collection.
	 * @return
	 */
	AdminConfiguration getConfiguration();

	/**
	 * Community config needs to be created on the application start-up for the very first time.
	 * All other time it will be updated.
	 * @param configurationRequest
	 * @return : Community Configuration Object.
	 * 
	 */
	AdminConfiguration updateConfiguration(AdminConfiguration configurationRequest) throws InvocationTargetException, IllegalAccessException;

	List<String> getReferralOptions();


    String getTermsAndCondition();
}
