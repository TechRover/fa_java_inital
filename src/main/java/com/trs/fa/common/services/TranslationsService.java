package com.trs.fa.common.services;


import java.util.Map;

public interface TranslationsService {

    Map<String,String> findByNameAndLang(String name,String lang);
}
