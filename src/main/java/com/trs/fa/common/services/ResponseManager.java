package com.trs.fa.common.services;


import com.trs.fa.common.decorator.Response;
import org.springframework.http.HttpStatus;

public interface ResponseManager {
     Response getResponse(HttpStatus code, String status, String description);

     Response getOkResponse();

     Response getSuccessResponse();
     Response getSuccessResponse(String description);

     Response getCreatedResponse();
     Response getCreatedResponse(String description);

     Response getUpdatedResponse();
     Response getUpdatedResponse(String description);

     Response getDeletedResponse();
     Response getDeletedResponse(String description);

     Response getInvalidRequestResponse();
     Response getInvalidRequestResponse(String description);

     Response getInvalidDataResponse();

     Response getNotFoundResponse();
     Response getNotFoundResponse(String description);

     Response getErrorResponse(String message, HttpStatus statusCode);

     Response getInternalServerErrorResponse();
     Response getInternalServerErrorResponse(String description);

     Response getContinueResponse(String description);
}
