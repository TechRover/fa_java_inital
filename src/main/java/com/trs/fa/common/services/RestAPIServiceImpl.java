package com.trs.fa.common.services;


import com.trs.fa.common.constant.MessageConstants;
import com.trs.fa.common.exception.NotFoundException;
import com.trs.fa.common.model.RestAPIs;
import com.trs.fa.common.repository.RestAPIRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RestAPIServiceImpl implements RestAPIService {

    @Autowired
    RestAPIRepository communityAPIRepository;

    @Override
    public List<RestAPIs> getCommunityAPIs() {
        return communityAPIRepository.findAll();
    }

    @Override
    public RestAPIs save(RestAPIs communityAPI) {
        return communityAPIRepository.save(communityAPI);
    }

    @Override
    public RestAPIs updateCommunityAPIRoles(RestAPIs communityAPI, String id) throws NotFoundException {
        RestAPIs communityConfig = communityAPIRepository.findById(id).orElseThrow(()->new NotFoundException(MessageConstants.API_NOT_FOUND));
        communityConfig.setRoles(communityAPI.getRoles());
        return save(communityConfig);
    }

    @Override
    public boolean hasAccess(List<String> roles, String api) {
        List<RestAPIs> access = communityAPIRepository.findByNameAndRolesIn(api, roles);
        return !access.isEmpty();
    }
}
