package com.trs.fa.common.services;

import com.trs.fa.common.model.Country;

import java.util.List;

public interface CountryService {

    List<Country> getAllCountry();
}
