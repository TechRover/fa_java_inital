package com.trs.fa.common.services;


import com.google.common.io.Resources;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.trs.fa.common.model.Translations;
import com.trs.fa.common.repository.TranslationRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class TranslationsServiceImpl implements TranslationsService {

    @Autowired
    TranslationRepository translationRepository;

    @Override
    public Map<String, String> findByNameAndLang(String name, String lang) {
        Map<String,String> fromFile = loadTranslationFileOrReturnBlank(name,lang);
        Map<String,String> fromDb = translationRepository.findByNameAndLang(name,lang).orElse(new Translations()).getTranslation();
        log.info("From Database {} ", fromDb);
        log.info("Full Query {} ", translationRepository.findByNameAndLang(name,lang).get());
        //Overrider Defaults
        for(String key : fromDb.keySet()){
            fromFile.put(key,fromDb.get(key));
        }
        return fromFile;
    }

    private Map<String,String>  loadTranslationFileOrReturnBlank(String name,String lang){
        try{
            URL url = Resources.getResource("translations/"+name+"."+lang+".json");
            String text = Resources.toString(url, StandardCharsets.UTF_8);
            Type type = new TypeToken<Map<String, String>>(){}.getType();
            Map<String,String> data = new Gson().fromJson(text,type);
            return data;
        }catch (Exception e){
            return new HashMap<String,String>();
        }

    }
}
