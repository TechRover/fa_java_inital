package com.trs.fa.common.services;

import com.trs.fa.common.exception.NotFoundException;
import com.trs.fa.common.model.RestAPIs;

import java.util.List;

public interface RestAPIService {

    List<RestAPIs> getCommunityAPIs();

    RestAPIs save(RestAPIs communityAPI);

    RestAPIs updateCommunityAPIRoles(RestAPIs communityAPI, String id) throws NotFoundException;

    boolean hasAccess(List<String> roles, String api);

}
