package com.trs.fa.common.services;

import com.trs.fa.common.model.Country;
import com.trs.fa.common.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CountryServiceImpl implements CountryService{

    @Autowired
    CountryRepository countryRepository;

    @Override
    public List<Country> getAllCountry() {
        return countryRepository.findBySoftDeleteIsFalse();
    }
}
