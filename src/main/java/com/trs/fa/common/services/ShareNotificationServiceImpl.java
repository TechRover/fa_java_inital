package com.trs.fa.common.services;

import com.trs.fa.common.connector.AuthConnector;
import com.trs.fa.common.publishers.PushNotificationPublisher;
import com.trs.fa.common.utils.NotificationParser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class ShareNotificationServiceImpl implements ShareNotificationService {


    @Autowired
    AuthConnector authConnector;

    @Autowired
    ConfigurationService configurationService;

    @Autowired
    NotificationParser parser;

    @Autowired
    PushNotificationPublisher publisher;

}
