package com.trs.fa.common.services;

import com.trs.fa.common.model.AdminConfiguration;
import com.trs.fa.common.repository.AdminConfigurationRepository;
import com.trs.fa.common.utils.NullAwareBeanUtilsBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

@Service
public class ConfigurationServiceImpl implements ConfigurationService {

	@Autowired
	AdminConfigurationRepository configRepository;

	@Autowired
	NullAwareBeanUtilsBean beanUtilsBean;

	@Override
	public AdminConfiguration getConfiguration() {
		List<AdminConfiguration> configurationList =  configRepository.findAll();
		if(configurationList.size() == 0){
			return new AdminConfiguration();
		}
		return configurationList.get(0);
	}

	@Override
	public AdminConfiguration updateConfiguration(AdminConfiguration configurationRequest) throws InvocationTargetException, IllegalAccessException {
		AdminConfiguration config = getConfiguration();
		new NullAwareBeanUtilsBean().copyProperties(config,configurationRequest);
		return configRepository.save(config);
	}

	@Override
	public List<String> getReferralOptions() {
		return null;
	}

	@Override
	public String getTermsAndCondition() {
		return null;
	}


}
