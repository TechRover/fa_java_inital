package com.trs.fa.common.services;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.google.common.collect.Lists;
import com.trs.fa.common.constant.MessageConstants;
import com.trs.fa.common.decorator.DeleteMediaRequest;
import com.trs.fa.common.decorator.FileUrlWithThumbnail;
import com.trs.fa.common.enums.StorageServiceType;
import com.trs.fa.common.exception.AuthException;
import com.trs.fa.common.exception.InvalidRequestException;
import com.trs.fa.common.model.AdminConfiguration;
import com.trs.fa.common.model.DeletedMediaLog;
import com.trs.fa.common.repository.DeletedMediaLogRepository;
import com.trs.fa.common.utils.S3Utils;
import com.trs.fa.lib.cloudstorage.GoogleStorageManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import static com.trs.fa.common.constant.MessageConstants.FILE_IS_NOT_PRESENT;

@Service
@Slf4j
public class FileUploadServiceImpl implements FileUploadService{

    private static final String THUMB_PREFIX = "thumb_";

    @Autowired
    ConfigurationService configService;

    @Autowired
    DeletedMediaLogRepository deleteMediaLogRepository;

    @Override
    public FileUrlWithThumbnail getMediaUrl(MultipartFile file, String userId) {
        AdminConfiguration configuration = configService.getConfiguration();
        if(file==null) {
            throw new InvalidRequestException(FILE_IS_NOT_PRESENT);
        }
        return uploader(file,configuration,"", S3Utils.generateRandomName()+"_"+userId+"_"+file.getOriginalFilename());
    }

    @Override
    public void deleteMediaFile(DeleteMediaRequest deleteMediaRequest, String userId) {

        AdminConfiguration configuration = configService.getConfiguration();

        DeletedMediaLog log = new DeletedMediaLog();

        boolean allow = false;
        if(allowMediaDelete(deleteMediaRequest.getLink(),userId,1)){
            allow = true;
            log.setLink(deleteMediaRequest.getLink());
            S3Utils.deleteFileFromS3(configuration,deleteMediaRequest.getLink());
        }
        if(allowMediaDelete(deleteMediaRequest.getThumbnailLink(),userId,2)){
            allow = true;
            log.setThumbnailLink(deleteMediaRequest.getThumbnailLink());
            S3Utils.deleteFileFromS3(configuration,deleteMediaRequest.getThumbnailLink());
        }
        if(!allow){
            throw new AuthException(MessageConstants.UNAUTHORIZED_ACCESS);
        }
        log.setUserId(userId);
        log.setDate(new Date());
        deleteMediaLogRepository.save(log);
    }
    private boolean allowMediaDelete(String link,String userId,int length){
        if(StringUtils.isEmpty(link)){
            return false;
        }
        if(StringUtils.isEmpty(userId)){
            return true;
        }
        String fileName = FilenameUtils.getName(link);
        String[] s = fileName.split("_");

        if(s.length>length){
            return s[length].equals(userId);
        }
        return false;
    }



    private FileUrlWithThumbnail uploader(MultipartFile file, AdminConfiguration configuration, String fileNameExtra, String fileName) {

        String extension = FilenameUtils.getExtension(file.getOriginalFilename());

        List<String> imageExtensions = Lists.newArrayList("jpeg", "jpg", "png");

        if (imageExtensions.contains(extension)) {
            return imageUpload(file, configuration, fileNameExtra,fileName);
        }
        return fileUpload(file, configuration, fileNameExtra,fileName);

    }

    private FileUrlWithThumbnail fileUpload(MultipartFile file, AdminConfiguration configuration, String fileNameExtra, String fileName){
        String imageName=null;
        try {
            fileName = fileNameExtra + fileName;

            byte [] byteFile=file.getBytes();
            imageName = uploadFile(fileName,byteFile,configuration);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new FileUrlWithThumbnail(imageName,null);
    }


    String uploadFile(String fileName,byte[] data,AdminConfiguration adminConfiguration) throws IOException {
        if(adminConfiguration.getStorageServiceType().equals(StorageServiceType.AWS_S3)){
            return S3Utils.storeFileInAwsS3(adminConfiguration, fileName, data,CannedAccessControlList.PublicRead);
        }else{
            return new GoogleStorageManager(adminConfiguration).uploadFile(fileName,data,null);
        }
    }

    private FileUrlWithThumbnail imageUpload(MultipartFile file, AdminConfiguration configuration, String fileNameExtra, String fileName) {
        try {
            String thumbName = THUMB_PREFIX +fileName;
            if(!StringUtils.isEmpty(fileNameExtra)){
                fileName = fileNameExtra + fileName;
                thumbName = fileNameExtra + thumbName;
            }

            double scale;
            double thumbScale = configuration.getImageThumbnailGenerateScale();
            double imageMaxSize = configuration.getImageMaxSize();
            if(imageMaxSize<0.5){
                imageMaxSize = 1;
            }

            byte[] byteFile;
            byte[] thumbnail;

            double size = getFileSizeMegaBytes(file);
            log.info("Original image size : "+size);
            if(size>imageMaxSize){
                scale = 100/size;
                if(scale<1){
                    scale = 1;
                }
                thumbScale = (scale*thumbScale)/100;

                log.info("scale : "+scale);
                log.info("thumbScale : "+thumbScale);

                byteFile = getThumbnailImage(file,scale);
                thumbnail = getThumbnailImage(file,thumbScale);
            }else {
                byteFile = file.getBytes();
                thumbnail = getThumbnailImage(file,thumbScale);
            }

            String originalImage = uploadFile( fileName, byteFile,configuration);
            String thumbnailImage =  uploadFile( thumbName, thumbnail,configuration);
            log.info("OriginalImage -> {} ",originalImage);
            log.info("ThumbnailImage -> {} ",thumbnailImage);
            return new FileUrlWithThumbnail(originalImage,thumbnailImage);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private byte[] getThumbnailImage(MultipartFile file, double scale) throws IOException {
        BufferedImage originalImage = ImageIO.read(file.getInputStream());
        int height =(int) (originalImage.getHeight() * scale / 100);
        int width = (int) (originalImage.getWidth() * scale / 100);

        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        img.createGraphics().drawImage(ImageIO.read(file.getInputStream()).getScaledInstance(width,height, Image.SCALE_SMOOTH),0,0, null);
        ByteArrayOutputStream outputFile = new ByteArrayOutputStream();
        ImageIO.write(img,"jpg",outputFile);

        return outputFile.toByteArray();

    }

    private static double getFileSizeMegaBytes(MultipartFile file) {
        return (double) file.getSize() / (1024 * 1024);
    }
}
