package com.trs.fa.common.services;

import com.trs.fa.common.decorator.DeleteMediaRequest;
import com.trs.fa.common.decorator.FileUrlWithThumbnail;
import org.springframework.web.multipart.MultipartFile;

public interface FileUploadService {

    FileUrlWithThumbnail getMediaUrl(MultipartFile file, String userId);

    void deleteMediaFile(DeleteMediaRequest deleteMediaRequest, String userId);
}
