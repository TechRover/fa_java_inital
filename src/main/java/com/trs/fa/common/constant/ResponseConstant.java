package com.trs.fa.common.constant;

public class ResponseConstant {

    //Response description
    public static final String DELETED_DESCRIPTION = "DELETED_DESCRIPTION";
    public static final String CREATED_DESCRIPTION = "CREATED_DESCRIPTION";
    public static final String UPDATE_DESCRIPTION = "UPDATE_DESCRIPTION";
    public static final String INVALID_REQUEST_DESCRIPTION = "INVALID_REQUEST_DESCRIPTION";
    public static final String INVALID_DATA_DESCRIPTION = "INVALID_DATA_DESCRIPTION";
    public static final String NOT_FOUND_DESCRIPTION = "NOT_FOUND_DESCRIPTION";
    public static final String OK_DESCRIPTION = "OK_DESCRIPTION";

    //ResponseMessage status
    public static final String DELETED = "DELETED";
    public static final String SUCCESS = "SUCCESS";
    public static final String VERIFY = "VERIFY";
    public static final String UPDATED = "UPDATED";
    public static final String ERROR = "ERROR";
    public static final String FAIL = "FAIL";
    public static final String OK = "OK";
    public static final String CONTINUE = "CONTINUE";

}
