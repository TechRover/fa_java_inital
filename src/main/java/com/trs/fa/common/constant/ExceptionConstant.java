package com.trs.fa.common.constant;

public class ExceptionConstant {

    public static final String DELETE_ERROR = "Error deleting given information";
    public static final String NOT_FOUND_ERROR = "No record found";
    public static final String INVALID_REQUEST_ERROR = "Invalid request found";
    public static final String ALREADY_EXISTS_ERROR = "Record already exists";
    public static final String MISSING_PARAMETER_ERROR = "Parameter not found";
    public static final String INVALID_DATA_ERROR = "Invalid data found";
    public static final String RECORD_ALREADY_EXISTS_ERROR = "Record already exists";
    public static final String NOT_APPROVED_MAIN_ERROR = "Sorry! Admin is not approved your registration request yet";
    public static final String NOT_APPROVED_SUB_ERROR = "Sorry! Admin is not approved sub member's registration request yet";
    public static final String REJECTED_MAIN_ERROR = "Sorry! Admin is rejected your registration request.Please update your account and try again later!";
    public static final String REJECTED_SUB_ERROR = "Sorry! Admin is rejected sub member's registration request.Please update sub member's account and try again later!";
    public static final String ALREADY_EXISTS_MEMBERSHIP_ERROR = "This member already have other membership";
    public static final String ALREADY_EXISTS_AS_SUB_MEMBER_ERROR = "This member is already exists in your membership";
    public static final String ALREADY_EXISTS_AS_SELF_MEMBER_ERROR = "You cannot add yourself as sub member";
    public static final String ALREADY_VERIFIED  = "You are already verified";
    public static final String RECORD_DELETED_ERROR = "Sorry! you are not authorized person to login, please contact your support team";

    public static final String AUTHENTICATION_ERROR = "Authentication error";
    public static final String TOKEN_NOT_FOUND = "Token not found";

    public static final String SMS_CONFIG_DISABLED_ERROR = "SMS feature is disabled by administrator";
    public static final String EMAIL_CONFIG_DISABLED_ERROR = "Email feature is disabled by administrator";

}
