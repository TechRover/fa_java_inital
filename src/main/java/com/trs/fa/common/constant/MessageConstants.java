package com.trs.fa.common.constant;


/**
 * Contains the Constants
 * @author TechRover Solution
 * @version 1.0
 * @since 2019-05-30
 */
public class MessageConstants {


	private MessageConstants() {
	}
	public static final String USER_DATA_KEY = "userData";
	public static final String PASS_WORD_KEY ="password";
	public static final String NEW_PASS_WORD_KEY = "newPassword";
	public static final String ACCESS_TOKEN_KEY = "accessToken";
	public static final String EXPIRES_IN_KEY = "expiresIn";
	public static final String TOKEN_TYPE_KEY = "tokenType";
	public static final String REFRESH_TOKEN_KEY = "refreshToken";
	public static final String ID_TOKEN_KEY = "idToken";
	public static final String AUTHENTICATION_PATH_KEY = "user";

	public static final String INVALID_DATA = "Invalid data found";
	public static final String NO_RECORD_FOUND = "No record found";


	public static final String TIME_RESERVED = "TIME_RESERVED";
	public static final String CAN_NOT_BOOK_PASS_APPOINMENT = "CAN_NOT_BOOK_PASS_APPOINMENT";
	public static final String APPOINTMENT_RESERVED_SUCCESSFULLY = "APPOINTMENT_RESERVED_SUCCESSFULLY";


	public static final String APPOINTMENT_ALREADY_EXISTS = "APPOINTMENT_ALREADY_EXISTS";
	public static final String APPOINTMENT_NOT_FOUND = "APPOINTMENT_NOT_FOUND";
	public static final String APPOINTMENT_ADDED_SUCCESSFULLY = "APPOINTMENT_ADDED_SUCCESSFULLY";
	public static final String APPOINTMENT_UPDATED_SUCCESSFULLY = "APPOINTMENT_UPDATED_SUCCESSFULLY";
	public static final String APPOINTMENT_DELETED_SUCCESSFULLY = "APPOINTMENT_DELETED_SUCCESSFULLY";
	public static final String APPOINTMENT_CANCELED_SUCCESSFULLY = "APPOINTMENT_CANCELED_SUCCESSFULLY";
	public static final String DOCUMENT_SHARED = "DOCUMENT_SHARED_SUCCESSFULLY";

	public static final String APPOINTMENT_CONFIRMED_SUCCESSFULLY = "APPOINTMENT_CONFIRMED_SUCCESSFULLY";

	public static final String SERVICE_TYPE_ALREADY_EXISTS = "SERVICE_TYPE_ALREADY_EXISTS";
	public static final String SERVICE_TYPE_NOT_FOUND = "SERVICE_TYPE_NOT_FOUND";
	public static final String SERVICE_TYPE_ADDED_SUCCESSFULLY = "SERVICE_TYPE_ADDED_SUCCESSFULLY";
	public static final String SERVICE_TYPE_DELETED_SUCCESSFULLY = "SERVICE_TYPE_DELETED_SUCCESSFULLY";
	public static final String SERVICE_TYPE_UPDATED_SUCCESSFULLY = "SERVICE_TYPE_UPDATED_SUCCESSFULLY";

	public static final String SERVICE_REASON_SAVED_SUCCESSFULLY = "SERVICE_REASON_SAVED_SUCCESSFULLY";


	public static final String ROLE_CREATED = "Role created successFully";
	public static final String ROLE_UPDATE = "Role successFully Updated";
	public static final String ROLE_FOUND = "Role Found  successFully";
	public static final String ROLE_NOT_FOUND = "Role Not Found";
	public static final String ROLE_DELETE = "Role Delete successFully";
	public static final String ROLE_GET_ALL = "Get All Role  successFully";
	public static final String ROLE_ID="Please Enter RoleId";
	public static final String ROLE_ALREADY_USED="You Can Not Delete This Role! This Role Is Already In Used";
	public static final String ROLE_IS_NOT_PROVIDED = "Role is not provided";
	public static final String RESPONSIBLE_PERSON_IS_NOT_SELECTED = "Responsible person is not selected";
	public static final String SELECT_MEMBER_FOR_REGISTRATION = "Please select member for registration";


	public static final String VOLUNTEER_NOT_FOUND = "Volunteer not found";
	public static final String ALREADY_EXISTS_AS_VOLUNTEER = "Already exists as volunteer";
	public static final String VOLUNTEER_ADDED_SUCCESSFULLY = "Volunteer added successfully";
	public static final String VOLUNTEER_ADDED_SUCCESSFULLY_EMAIL_SENT = "Volunteer added successfully and email verification link has been sent to %s email id";
	public static final String VOLUNTEER_REMOVED_SUCCESSFULLY = "Volunteer removed successfully";

	public static final String ADMIN_NOT_FOUND = "Admin not found";
	public static final String ALREADY_EXISTS_AS_ADMIN = "Already exists as admin";
	public static final String ADMIN_ADDED_SUCCESSFULLY = "Admin added successfully";
	public static final String ADMIN_ADDED_SUCCESSFULLY_EMAIL_SENT = "Admin added successfully and email verification link has been sent to %s email id";
	public static final String ADMIN_REMOVED_SUCCESSFULLY = "Admin removed successfully";

	public static final String USER_REGISTERED_SUCCESS ="User registered successfully";
	public static final String USER_CREATED = "User created successfully";
	public static final String USER_FOUND = "User Found  successFully";
	public static final String USER_NOT_FOUND = "User not found";
	public static final String VENDOR_NOT_FOUND = "Vendor not found";
	public static final String SUB_USER_NOT_FOUND = "Sub user not found";
	public static final String USER_DELETE = "User Delete successFully";
	public static final String USER_GET_ALL = "Get All User  successFully";
	public static final String USER_NAME_ENTER="Please enter valid user-name";
	public static final String USER_ID="Please Enter UserId";
	public static final String USER_ALREADY_REGISTERED ="User already registered";
	public static final String USER_ALREADY_EXISTS ="User already exists";
	public static final String PLEASE_SEND_VALID_EMAIL_ADDRESS ="Please send valid email address";
	public static final String USER_ENTER_EMAIL_OR_PHONE_NUMBER ="Please enter email-userId or phone-number";
	public static final String USER_NOT_APPROVED="User not approved yet";
	public static final String YOUR_ACCOUNT_CREATED = "Your account is created successfully";
	public static final String GUEST_REGISTRATION_NOT_ALLOWED = "Guest registration is not allowed";



	public static final String LOGIN_USERNAME_WRONG = "Please enter Correct UserName";
	public static final String LOGIN_PAASWORD_WRONG = "Please enter Correct Password";
	public static final String LOGIN_SUCCESSFULLY = "Login successfully";
	public static final String LOGOUT_SUCCESSFULLY = "Logout successfully";
	public static final String LOGIN_USERNAME_BLANK = "Please enter UserName";
	public static final String LOGIN_PASSWORD_BLANK = "Please enter Password";
	public static final String LOGIN_OTP_BLANK = "Please enter OTP";
	public static final String LOGIN_OTP_WRONG="Please enter Correct OTP";
	public static final String LOGIN_USERNAME_OR_TOP="Please enter Valid UserName or OTP";


	public static final String USER_ID_IS_NOT_VALID="Please enter valid user-userId";
	public static final String USER_VERIFY="User verified successfully";
	public static final String TOKEN_IS_NOT_VALID="Please enter valid token";
	public static final String API_IS_NOT_VALID="Please enter valid API token";
	public static final String TOKEN_NOT_FOUND="Token not found";

	public static final String USER_ID_TOKEN="Please enter user-userId or token";
	public static final String OTP_NOT_VERIFY="Please enter valid one time password";
	public static final String INVALID_OTP="Please enter valid OTP";
	public static final String INVALID_INPUT="Please enter valid input";
	public static final String INVALID_CREDENTIALS ="Please enter valid credentials";
	public static final String OTP_EXPIRED ="Your OTP is expired!";

	public static final String VERIFICATION_CODE_SEND="Verification code sent Successfully";
	public static final String PASSWORD_CHANGED_SUCCESSFULLY ="Password changed successfully";
	public static final String PASSWORD_RESET_LINK_HAS_BEEN_SENT_TO_EMAIL ="Password reset link has been sent to %s email";

	public static final String AUTH_CONFIG_CREATED="Auth Configuration Created Successfully";
	public static final String AUTH_CONFIG_UPDATE="Auth Configuration Updated Successfully";
	public static final String AUTH_CONFIG_VALID_ID="Please enter Auth Configuration Valid Id";
	public static final String AUTH_CONFIG_UPDATE_VALID_ID="Please enter Auth Configuration Update Id";
	public static final String AUTH_CONFIG_ID_INSERT="Please enter Auth ConfigurationId";
	public static final String AUTH_CONFIG_NOT_FOUND="Please Set the Auth Config";

	public static final String WEB_TOKEN_NOT_NULL="WebToken cannot be missing.";


	public static final String UNAUTHORIZED_ACCESS="You are not authorized person to access data";
	public static final String API_SUCCESS="Successfully retrieved list";
	public static final String API_NOT_AUTHORIZED="You are not authorized to view the resource";
	public static final String API_FORBIDDEN="Accessing the resource you were trying to reach is forbidden";
	public static final String API_NOT_FOUND="API is not found";


	public static final String INVALID_AGE_FOUND = "Invalid age found";
	public static final String SERVER_CONFIG_ERROR = "Server is not configured properly";


	public static final String RECORD_ALREADY_EXISTS ="Record already exists";
	public static final String FIELD_NOT_NULL="UserName,MemberId and Email,PhoneNumber Compulsory";
	public static final String EMAIL_ALREADY_IN_USED="Email is already in used";
	public static final String USER_NAME_ALREADY_IN_USED="User-name is already in used";
	public static final String MEMBER_NUMBER_ALREADY_IN_USED="Member-number is already in used";
	public static final String PHONE_NUMBER_ALREADY_IN_USED ="Phone-number is already in used";

	public static final String AUTHORIZATION_IS_NOT_PRESENT_IN_REQUEST ="Authentication is not present in the request";
	public static final String AUTHORIZATION_IS_NOT_VALID="Authorization is not valid";
	public static final String ROLE_IS_NOT_ALLOWED="Role is not allowed";
	public static final String INVALID_TOKEN_SIGNATURE="Invalid token signature";
	public static final String SESSION_EXPIRED ="Session expired";
	public static final String INVALID_TOKEN="Invalid token";

	public static final String LIMITED_REGISTRATION_PER_PERSON = "You can't add more than %s persons";
	public static final String LIMITED_REGISTRATION_PER_EVENT = "Event registration capacity is full. Only %s persons can register";
	public static final String NO_ONE_PERSONS_CAN_REGISTER = "Event registration capacity is full. No one can register";

	public static final String LIMITED_BULK_REGISTRATION = "Only %s booth(s) remaining in %s booth";
	public static final String BULK_REGISTRATION_FULL = "\"%s\" booth registration capacity is full";

	public static final String CATEGORY_REGISTRATION_CAPACITY_IS_FULL = "\"%s\" category registration capacity is full";


	public static final String EVENT_STATUS_SAVED_SUCCESSFULLY ="Event status saved successfully";

	public static final String INVALID_LOCATION = "You have entered invalid location";

	public static final String EVENT_NOT_FOUND="Event not found";
	public static final String EVENT_CREATED_SUCCESSFULLY="Event created successfully";
	public static final String EVENT_UPDATED_SUCCESSFULLY ="Event updated successfully";
	public static final String EVENT_DELETED_SUCCESSFULLY="Event deleted successfully";

	public static final String EVENT_COPIED_SUCCESSFULLY ="Event copied successfully";

	public static final String AMOUNT_IS_NOT_MATCH_WITH_CATEGORY_AMOUNT ="Your entered amount is not match with category amount";
	public static final String AMOUNT_IS_NOT_MATCH_WITH_SPONSORSHIP_CATEGORY_AMOUNT ="Your entered amount is not match with sponsorship category amount";
	public static final String AMOUNT_IS_NOT_MATCH_WITH_DONATION_CATEGORY_AMOUNT ="Your entered amount is not match with donation category amount";
	public static final String SPONSORSHIP_DONATION_CATEGORY_NOT_FOUND ="Event sponsorship/donation category not found";
	public static final String SPONSORSHIP_CATEGORY_NOT_FOUND ="Event sponsorship category not found";
	public static final String DONATION_CATEGORY_NOT_FOUND ="Event donation category not found";
	public static final String SPONSORSHIP_CATEGORY_ADDED_SUCCESSFULLY ="Event sponsorship category added successfully";
	public static final String SPONSORSHIP_CATEGORY_UPDATED_SUCCESSFULLY ="Event sponsorship category updated successfully";
	public static final String CATEGORY_DELETED_SUCCESSFULLY ="Category deleted successfully";

	public static final String DONATION_CATEGORY_ADDED_SUCCESSFULLY ="Event donation category added successfully";
	public static final String DONATION_CATEGORY_UPDATED_SUCCESSFULLY ="Event donation category updated successfully";
	public static final String DONATION_CATEGORY_DELETED_SUCCESSFULLY ="Event donation category deleted successfully";

	public static final String AMOUNT_DONATED_SUCCESSFULLY ="Amount donated successfully";
	public static final String SPONSORSHIP_PURCHASED_SUCCESSFULLY ="Sponsorship purchased successfully";

	public static final String MARKETING_MATERIAL_NOT_ALLOWED = "Marketing material is not allowed";
	public static final String BANNER_IS_NOT_ALLOWED = "Site banner is not allowed";
	public static final String VIDEO_IS_NOT_ALLOWED = "Video is not allowed";
	public static final String OTHER_MATERIAL_IS_NOT_ALLOWED = "Other material is not allowed";

	public static final String BANNERS_ARE_ALLOWED = "Only %s site banners are allowed";
	public static final String VIDEOS_ARE_ALLOWED = "Only %s videos are allowed";
	public static final String OTHER_MATERIALS_ARE_ALLOWED = "Only %s other materials are allowed";

	public static final String BANNER_IS_ALLOWED = "Only one site banner is allowed";
	public static final String VIDEO_IS_ALLOWED = "Only one video is allowed";
	public static final String OTHER_MATERIAL_IS_ALLOWED = "Only one other material is allowed";

	public static final String GETTER_NOT_FOUND = "%s getter not found";

	public static final String EVENT_TYPE_ALREADY_EXISTS ="Event type already exists";
	public static final String EVENT_TYPE_NOT_FOUND ="Event type not found";
	public static final String EVENT_TYPE_ADDED_SUCCESSFULLY ="Event type added successfully";
	public static final String EVENT_TYPE_UPDATED_SUCCESSFULLY ="Event type updated successfully";
	public static final String EVENT_TYPE_DELETED_SUCCESSFULLY ="Event type deleted successfully";

	public static final String EVENT_CATEGORY_ALREADY_EXISTS ="Event category already exists";
	public static final String EVENT_CATEGORY_NOT_FOUND ="Event category not found";
	public static final String EVENT_CATEGORY_ADDED_SUCCESSFULLY ="Event category added successfully";
	public static final String EVENT_CATEGORY_UPDATED_SUCCESSFULLY ="Event category updated successfully";
	public static final String EVENT_CATEGORY_DELETED_SUCCESSFULLY ="Event category deleted successfully";

	public static final String EVENT_SEATING_CATEGORY_ADDED_SUCCESSFULLY = "Event seating category added successfully";
	public static final String EVENT_SEATING_CATEGORY_UPDATED_SUCCESSFULLY = "Event seating category updated successfully";
	public static final String EVENT_SEATING_CATEGORY_DELETED_SUCCESSFULLY = "Event seating category deleted successfully";
	public static final String EVENT_SEATING_CATEGORY_NOT_FOUND = "Event seating category not found";
	public static final String EVENT_SEATING_CATEGORY_NOT_SELECTED = "Event seating category not selected";

	public static final String AGE_GROUP_IS_NOT_DEFINED = "Age group is not defined for \"%s\" age";

	public static final String REFUND_RULE_ADDED_SUCCESSFULLY = "Refund rule added successfully";
	public static final String REFUND_RULE_UPDATED_SUCCESSFULLY = "Refund rule updated successfully";
	public static final String REFUND_RULE_DELETED_SUCCESSFULLY = "Refund rule deleted successfully";
	public static final String REFUND_RULE_NOT_FOUND = "Refund rule not found";

	public static final String ACCOMMODATION_OPTION_ADDED_SUCCESSFULLY = "Accommodation option added successfully";
	public static final String ACCOMMODATION_OPTION_UPDATED_SUCCESSFULLY = "Accommodation option updated successfully";
	public static final String ACCOMMODATION_OPTION_DELETED_SUCCESSFULLY = "Accommodation option deleted successfully";
	public static final String ACCOMMODATION_OPTION_NOT_FOUND = "Accommodation option not found";


	public static final String DYNAMIC_FIELD_ADDED_SUCCESSFULLY ="Dynamic field added successfully";
	public static final String DYNAMIC_FIELD_UPDATED_SUCCESSFULLY ="Dynamic field updated successfully";
	public static final String DYNAMIC_FIELD_DELETED_SUCCESSFULLY ="Dynamic field deleted successfully";
	public static final String DYNAMIC_FIELD_NOT_FOUND ="Dynamic field not found";

	public static final String PURCHASABLE_ITEM_ADDED_SUCCESSFULLY ="Purchasable item added successfully";
	public static final String PURCHASABLE_ITEM_UPDATED_SUCCESSFULLY ="Purchasable item updated successfully";
	public static final String PURCHASABLE_ITEM_DELETED_SUCCESSFULLY ="Purchasable item deleted successfully";
	public static final String PURCHASABLE_ITEM_NOT_FOUND ="Purchasable item not found";

	public static final String PLEASE_PROVIDE_EVENT_RULE_NAME ="Please provide category name";
	public static final String PLEASE_PROVIDE_EVENT_RULE_MIN_VALUE ="Please provide event rule min value";
	public static final String PLEASE_PROVIDE_EVENT_RULE_MAX_VALUE ="Please provide event rule max value";

	public static final String EVENT_DYNAMIC_FIELD_UPDATED = "Event dynamic fields saved successfully";
	public static final String EVENT_PRICING_UPDATED = "Event pricing saved successfully";
	public static final String EVENT_CONFIG_UPDATED = "Event configuration saved successfully";
	public static final String EVENT_CONFIG_NOT_SET_YET = "Event configuration not set yet";
	public static final String CONFIGURATION_KEYS_NOT_SET_YET = "Event configuration keys not set yet";
	public static final String SPONSORSHIP_RANGE_TYPE_NOT_NULL = "In event configuration, sponsorship range type must be not null";

	public static final String NOTIFICATION_SCHEDULE_NOT_FOUND = "Notification schedule not found";
	public static final String NOTIFICATION_SCHEDULE_CANCELED = "Notification schedule canceled successfully";
	public static final String NOTIFICATION_SCHEDULE_ADDED = "Notification schedule added successfully";
	public static final String NOTIFICATION_SCHEDULE_UPDATED = "Notification schedule updated successfully";
	public static final String NOTIFICATION_SCHEDULE_DELETED = "Notification schedule deleted successfully";
	public static final String NOTIFICATION_SCHEDULE_IS_PARTIAL_DELETED = "Notification schedule has canceled all the other incomplete schedule.";


	public static final String EVENT_IS_UNDER_REVIEW = "Event is in under review, kindly check after some time";

	public static final String FILE_IS_NOT_PRESENT = "Required request part 'file' is not present";
	public static final String IMAGE_UPLOADED_SUCCESSFULLY = "Image uploaded successfully";
	public static final String DOCUMENT_UPLOADED_SUCCESSFULLY = "Document uploaded successfully";
	public static final String DOCUMENT_DELETED_SUCCESSFULLY = "Document deleted successfully";
	public static final String FILE_UPLOAD_CONFIGURATION_NOT_SET_YET = "File upload configuration not set yet";
	public static final String IMAGE_UPDATED_SUCCESSFULLY = "Image saved successfully";

	public static final String FILE_UPLOADED_SUCCESSFULLY = "File uploaded successfully";
	public static final String FILE_DELETED_SUCCESSFULLY = "File deleted successfully";
	public static final String FILE_UPDATED_SUCCESSFULLY = "File updated successfully";

	public static final String DONATION_IS_NOT_ALLOWED = "Donation is not allowed";
	public static final String SPONSORSHIP_IS_NOT_ALLOWED = "Sponsorship is not allowed";

	public static final String PLEASE_SELECT_AT_LEAST_ONE_PLAN = "Please select at least one plan";
	public static final String PLEASE_SELECT_AT_LEAST_ONE_BOOTH = "Please select at least one booth";
	public static final String BOOTH_IS_NOT_ALLOWED = "Vendor booth selection is not allowed";
	public static final String VENDOR_EXPO_NOT_FOUND = "Vendor booth category not found";
	public static final String VENDOR_EXPO_CREATED_SUCCESSFULLY = "Vendor booth category created successfully";
	public static final String VENDOR_EXPO_UPDATED_SUCCESSFULLY = "Vendor booth category updated successfully";
	public static final String VENDOR_EXPO_DELETED_SUCCESSFULLY = "Vendor booth category deleted successfully";

	public static final String SELECTED_FOOD_NOT_FOUND = "Selected food \"%s\" not available";
	public static final String SELECTED_FOOD_QTY_INVALID = "Please select quantity for \"%s\"";
	public static final String MULTI_FOOD_SELECT_NOT_AVAILABLE = "Multiple food selection is not allowed";
	public static final String FOOD_IS_NOT_ALLOWED = "Food selection is not allowed";

	public static final String SESSION_CREATED_SUCCESSFULLY = "Session created successfully";
	public static final String SESSION_UPDATED_SUCCESSFULLY = "Session updated successfully";
	public static final String SESSION_DELETED_SUCCESSFULLY = "Session deleted successfully";
	public static final String SESSION_NOT_FOUND = "Session not found";

	public static final String DISCOUNT_CODE_ALREADY_EXISTS = "Discount code already exists";
	public static final String DISCOUNT_CODE_NOT_FOUND = "Discount code not found";
	public static final String DISCOUNT_CODE_ADDED_SUCCESSFULLY = "Discount code added successfully";
	public static final String DISCOUNT_CODE_UPDATED_SUCCESSFULLY = "Discount code updated successfully";
	public static final String DISCOUNT_CODE_DELETED_SUCCESSFULLY = "Discount code deleted successfully";

	public static final String DISCOUNT_CODE_DATE_LIMIT = "You can only use this discount code between %s to %s date";
	public static final String DISCOUNT_CODE_LIMIT = "Discount code usage limit is reached";
	public static final String DISCOUNT_CODE_IS_EXPIRED = "Discount code is expired";
	public static final String DISCOUNT_CODE_CANNOT_BE_APPLICABLE = "Discount code cannot be applicable";

	public static final String EVENT_LIKED_SUCCESSFULLY = "Event liked successfully";
	public static final String EVENT_DISLIKED_SUCCESSFULLY = "Event disliked successfully";

	public static final String EVENT_INTERESTED_SUCCESSFULLY = "Event added as interested successfully";
	public static final String EVENT_UNINTERESTED_SUCCESSFULLY = "Event removed from interested successfully";


	public static final String MIN_DATE_NOT_FOUND = "Please provide minimum valid date";
	public static final String MAX_DATE_NOT_FOUND = "Please provide maximum valid date";
	public static final String INVALID_DATE = "Please provide valid date";
	public static final String INVALID_YEAR = "Please provide valid year";
	public static final String INVALID_DATE_TIME = "Please provide valid date and time";
	public static final String INVALID_DATE_FORMAT = "Please provide valid date format";
	public static final String CANNOT_CHANGE_DATE_TIME = "You can not change event date and time";

	public static final String INVALID_DATE_TIME_IN_EVENT = "Please set valid date and time of event";

	public static final String DATE_TIME_MUST_BETWEEN_SPECIFIC_DATE_TIME = "Date and time must be between %s to %s date-time";
	public static final String SESSION_DATE_MUST_BE_SAME_AS_SCHEDULE_DATE = "Session date must be same as schedule date like %s";

	public static final String ALREADY_CHECKED_IN_TO_THIS_SESSION = "User already checked in to this session";
	public static final String ALREADY_CHECKED_IN_TO_THIS_EVENT = "User already checked in to this event";
	public static final String USER_ALREADY_CHECKED_IN = "User already checked in";
	public static final String USER_CHECKED_IN_DETAILS_NOT_FOUND = "User checked in details not found";
	public static final String SESSION_CHECKIN_CAPACITY_IS_FULL = "Session checkin capacity is full";

	public static final String SESSION_CHECKIN_SUCCESSFULLY = "Session checked in successfully";
	public static final String EVENT_CHECKIN_SUCCESSFULLY = "Event checked in successfully";
	public static final String USER_CHECKIN_SUCCESSFULLY = "User checked in successfully";

	public static final String PLEASE_SELECT_PURCHASABLE_ITEMS = "Please select purchasable item(s)";

	public static final String PURCHASABLE_ITEMS_REMAINING = "Only %s purchasable items remaining, you can't buy more than that";
	public static final String PURCHASABLE_ITEMS_REMAINING_ONE = "Only one purchasable item remaining, you can't buy more than that";
	public static final String PURCHASABLE_ITEMS_REMAINING_NO_ONE = "No one purchasable item remaining";

	public static final String VENDOR_QUESTION_ADDED_SUCCESSFULLY ="Vendor question added successfully";
	public static final String VENDOR_QUESTION_UPDATED_SUCCESSFULLY ="Vendor question updated successfully";
	public static final String VENDOR_QUESTION_DELETED_SUCCESSFULLY ="Vendor question deleted successfully";
	public static final String VENDOR_QUESTION_NOT_FOUND ="Vendor question not found";

	public static final String SELECTED_OPTION_IS_NOT_AVAILABLE ="Selected option \"%s\" is not available";

	public static final String CHAPTER_ADDED_SUCCESSFULLY ="Chapter added successfully";
	public static final String CHAPTER_UPDATED_SUCCESSFULLY ="Chapter updated successfully";
	public static final String CHAPTER_DELETED_SUCCESSFULLY ="Chapter deleted successfully";
	public static final String CHAPTER_NOT_FOUND ="Chapter not found";


	public static final String COMMITTEE_MEMBER_ADDED_SUCCESSFULLY = "Committee member added successfully";
	public static final String COMMITTEE_MEMBER_UPDATED_SUCCESSFULLY = "Committee member updated successfully";
	public static final String COMMITTEE_MEMBER_DELETED_SUCCESSFULLY ="Committee member deleted successfully";
	public static final String COMMITTEE_MEMBER_NOT_FOUND ="Committee member not found";
	public static final String COMMITTEE_MEMBER_ALREADY_EXISTS ="Committee member already exists";

	public static final String COMMUNITY_SAVED_SUCCESSFULLY ="Community saved successfully";
	public static final String COMMUNITY_NOT_FOUND ="Community not found";




	public static final String SUBJECT_ADDED_SUCCESSFULLY ="Subject added successfully";
	public static final String CONTACT_US_ADDED ="Thank you for contacting us, we will get back to you soon.";
	public static final String SUBJECT_UPDATED_SUCCESSFULLY ="Subject updated successfully";

	public static final String APPOINTMENT_NOTE_ALREADY_EXISTS ="APPOINTMENT_NOTE_ALREADY_EXISTS";
	public static final String APPOINTMENT_NOTE_NOT_FOUND ="APPOINTMENT_NOTE_NOT_FOUND";
	public static final String APPOINTMENT_NOTE_ADDED_SUCCESSFULLY ="APPOINTMENT_NOTE_ADDED_SUCCESSFULLY";
	public static final String APPOINTMENT_NOTE_UPDATED_SUCCESSFULLY ="APPOINTMENT_NOTE_UPDATED_SUCCESSFULLY";
	public static final String APPOINTMENT_NOTE_SAVED_SUCCESSFULLY ="APPOINTMENT_NOTE_SAVED_SUCCESSFULLY";
	public static final String APPOINTMENT_NOTE_DELETED_SUCCESSFULLY ="APPOINTMENT_NOTE_DELETED_SUCCESSFULLY";

	public static final String APPOINTMENT_DOCUMENT_ALREADY_EXISTS ="APPOINTMENT_DOCUMENT_ALREADY_EXISTS";
	public static final String APPOINTMENT_DOCUMENT_NOT_FOUND ="APPOINTMENT_DOCUMENT_NOT_FOUND";
	public static final String APPOINTMENT_DOCUMENT_ADDED_SUCCESSFULLY ="APPOINTMENT_DOCUMENT_ADDED_SUCCESSFULLY";
	public static final String APPOINTMENT_DOCUMENT_DELETED_SUCCESSFULLY ="APPOINTMENT_DOCUMENT_DELETED_SUCCESSFULLY";
	public static final String APPOINTMENT_DOCUMENT_UPDATED_SUCCESSFULLY ="APPOINTMENT_DOCUMENT_UPDATED_SUCCESSFULLY";

	public static final String PATIENT_DETAILS_ALREADY_EXISTS ="PATIENT_DETAILS_ALREADY_EXISTS";
	public static final String PATIENT_DETAILS_NOT_FOUND ="PATIENT_DETAILS_NOT_FOUND";
	public static final String PATIENT_IMPORTED_SUCESSFULLY ="PATIENT_IMPORTED_SUCESSFULLY";
	public static final String PATIENT_DETAILS_ADDED_SUCCESSFULLY ="PATIENT_DETAILS_ADDED_SUCCESSFULLY";
	public static final String PATIENT_DETAILS_DELETED_SUCCESSFULLY ="PATIENT_DETAILS_DELETED_SUCCESSFULLY";
	public static final String PATIENT_DETAILS_UPDATED_SUCCESSFULLY ="PATIENT_DETAILS_UPDATED_SUCCESSFULLY";
	public static final String PATIENT_DETAILS_SAVED_SUCCESSFULLY ="PATIENT_DETAILS_SAVED_SUCCESSFULLY";

	public static final String MEDICATION_DETAILS_ALREADY_EXISTS ="MEDICATION_DETAILS_ALREADY_EXISTS";
	public static final String MEDICATION_DETAILS_NOT_FOUND ="MEDICATION_DETAILS_NOT_FOUND";
	public static final String MEDICATION_DETAILS_ADDED_SUCCESSFULLY ="MEDICATION_DETAILS_ADDED_SUCCESSFULLY";
	public static final String MEDICATION_DETAILS_DELETED_SUCCESSFULLY ="MEDICATION_DETAILS_DELETED_SUCCESSFULLY";
	public static final String MEDICATION_DETAILS_UPDATED_SUCCESSFULLY ="MEDICATION_DETAILS_UPDATED_SUCCESSFULLY";
	public static final String MEDICATION_DETAILS_SAVED_SUCCESSFULLY ="MEDICATION_DETAILS_SAVED_SUCCESSFULLY";

	public static final String WELLNESS_JOURNAL_ALREADY_EXISTS ="WELLNESS_JOURNAL_ALREADY_EXISTS";
	public static final String WELLNESS_JOURNAL_NOT_FOUND ="WELLNESS_JOURNAL_NOT_FOUND";
	public static final String WELLNESS_JOURNAL_ADDED_SUCCESSFULLY ="WELLNESS_JOURNAL_ADDED_SUCCESSFULLY";
	public static final String WELLNESS_JOURNAL_SHARED_SUCCESSFULLY ="WELLNESS_JOURNAL_SHARED_SUCCESSFULLY";
	public static final String WELLNESS_JOURNAL_DELETED_SUCCESSFULLY ="WELLNESS_JOURNAL_DELETED_SUCCESSFULLY";
	public static final String WELLNESS_JOURNAL_UPDATED_SUCCESSFULLY ="WELLNESS_JOURNAL_UPDATED_SUCCESSFULLY";

	public static final String USER_DOCUMENT_NOT_FOUND="USER_DOCUMENT_NOT_FOUND";
	public static final String DOCUMENT_NOT_FOUND="DOCUMENT_NOT_FOUND";

	public static final String USER_DOCUMENT_CATEGORY_NOT_FOUND="USER_DOCUMENT_CATEGORY_NOT_FOUND";

	public static final String NEWS_ALREADY_EXISTS ="NEWS_ALREADY_EXISTS";
	public static final String NEWS_NOT_FOUND="NEWS_NOT_FOUND";
	public static final String NEWS_ADDED_SUCCESSFULLY ="NEWS_ADDED_SUCCESSFULLY";
	public static final String NEWS_DELETED_SUCCESSFULLY ="NEWS_DELETED_SUCCESSFULLY";
	public static final String NEWS_UPDATED_SUCCESSFULLY ="NEWS_UPDATED_SUCCESSFULLY";

	public static final String PRESCRIPTION_SAVED_SUCCESSFULLY="PRESCRIPTION_SAVED_SUCCESSFULLY";

	public static final String TODO_ALREADY_EXISTS ="TODO_ALREADY_EXISTS";
	public static final String TODO_NOT_FOUND="TODO_NOT_FOUND";
	public static final String TODO_ADDED_SUCCESSFULLY ="TODO_ADDED_SUCCESSFULLY";
	public static final String TODO_DELETED_SUCCESSFULLY ="TODO_DELETED_SUCCESSFULLY";
	public static final String TODO_UPDATED_SUCCESSFULLY ="TODO_UPDATED_SUCCESSFULLY";

	public static final String SUBSCRIPTION_ALREADY_EXISTS ="SUBSCRIPTION_ALREADY_EXISTS";
	public static final String SUBSCRIPTION_NOT_FOUND="SUBSCRIPTION_NOT_FOUND";
	public static final String SUBSCRIPTION_TIMELINE_NOT_FOUND="SUBSCRIPTION_TIMELINE_NOT_FOUND";
	public static final String SUBSCRIPTION_ADDED_SUCCESSFULLY ="SUBSCRIPTION_ADDED_SUCCESSFULLY";
	public static final String SUBSCRIPTION_DELETED_SUCCESSFULLY ="SUBSCRIPTION_DELETED_SUCCESSFULLY";
	public static final String SUBSCRIPTION_UPDATED_SUCCESSFULLY ="SUBSCRIPTION_UPDATED_SUCCESSFULLY";

	public static final String PLEASE_PROVIDE_SUBSCRIPTION ="PLEASE_PROVIDE_SUBSCRIPTION";
	public static final String PLEASE_PROVIDE_TIMELINE ="PLEASE_PROVIDE_SUBSCRIPTION_TIMELINE";
	public static final String PLEASE_PROVIDE_PAYMENT_METHOD ="PLEASE_PROVIDE_PAYMENT_METHOD";
	public static final String PLEASE_PROVIDE_BASIC_INFO ="PLEASE_PROVIDE_BASIC_INFO";
	public static final String PLEASE_PROVIDE_PROVIDER_INFO ="PLEASE_PROVIDE_PROVIDER_INFO";
	public static final String PLEASE_PROVIDE_PROFESSIONAL_TYPE ="PLEASE_PROVIDE_PROFESSIONAL_TYPE";

	public static final String BECOME_PROVIDER_SUCCESSFULLY ="BECOME_PROVIDER_SUCCESSFULLY";
	public static final String PLAN_UPDATED_SUCCESSFULLY ="PLAN_UPDATED_SUCCESSFULLY";
	public static final String PLAN_NOT_FOUND ="Plan not found";
	public static final String PROVIDER_NOT_FOUND ="PROVIDER_NOT_FOUND";
	public static final String PROVIDER_DELETED_SUCCESSFULLY ="PROVIDER_DELETED_SUCCESSFULLY";
	public static final String PROVIDER_STATUS_IS_NOT_ACTIVE ="PROVIDER_STATUS_IS_NOT_ACTIVE";
	public static final String PROVIDER_SUBSCRIPTION_IS_EXPIRED ="PROVIDER_SUBSCRIPTION_IS_EXPIRED";
	public static final String PROVIDER_PAYMENT_IS_REMAINING ="PROVIDER_PAYMENT_IS_REMAINING";
	public static final String PROVIDER_CAPACITY_IS_FULL ="PROVIDER_CAPACITY_IS_FULL";

	public static final String PROVIDER_ALREADY_HAVE_SUBSCRIPTION ="PROVIDER_ALREADY_HAVE_SUBSCRIPTION";

	public static final String PROFESSIONAL_ALREADY_EXISTS ="PROFESSIONAL_ALREADY_EXISTS";
	public static final String PROFESSIONAL_NOT_FOUND="PROFESSIONAL_NOT_FOUND";
	public static final String PROFESSIONAL_ADDED_SUCCESSFULLY ="PROFESSIONAL_ADDED_SUCCESSFULLY";
	public static final String PROFESSIONAL_DELETED_SUCCESSFULLY ="PROFESSIONAL_DELETED_SUCCESSFULLY";
	public static final String PROFESSIONAL_UPDATED_SUCCESSFULLY ="PROFESSIONAL_UPDATED_SUCCESSFULLY";
	public static final String PAYMENT_INVALID ="PAYMENT_INVALID";

	public static final String PLEASE_COMPLETE_PAYMENT_PROCESS ="PLEASE_COMPLETE_PAYMENT_PROCESS";

	public static final String PAYMENT_SERVICE_IS_NOT_AVAILABLE ="PAYMENT_SERVICE_IS_NOT_AVAILABLE";


	public static final String BLOCK_CALENDER_SAVED_SUCCESSFULLY = "BLOCK_CALENDER_SAVED_SUCCESSFULLY";
	public static final String BLOCK_CALENDER_DELETED_SUCCESSFULLY = "BLOCK_CALENDER_DELETED_SUCCESSFULLY";

	public static final String NOTIFICATION_NOT_SENT = "Notification not sent";

	// Heart Logs Messages .

	public static final String HEART_LOG_NOTFOUND ="HEART_LOG_NOTFOUND";
	public static final String HEART_LOG_CREATED ="HEART_LOG_CREATED";
	public static final String HEART_LOG_UPDATED ="HEART_LOG_UPDATED";


	public static final String RECORDING_NOT_FOUND ="RECORDING_NOT_FOUND";
	public static final String RECORDING_UPLOADED ="RECORDING_UPLOADED";
	public static final String RECORDING_DELETED ="RECORDING_DELETED";


	public static final String ACTIVE_SESSION_LIMIT_REACHED_PLEASE_CONTACT_ADMINISTRATOR ="Active session limit reached, Please contact administrator";
	public static final String ACTIVE_SESSION_NOT_FOUND ="Active session not found";

	public static final String REGISTRATION_DONE_SUCCESSFULLY ="Registration done successfully";
	public static final String USER_UPDATED_SUCCESSFULLY ="User updated successfully";
	public static final String USER_DELETED_SUCCESSFULLY ="User deleted successfully";
	public static final String USER_INVITED_SUCCESSFULLY ="User invited successfully";

	public static final String REFERENCE_CODE_SENT_SUCCESSFULLY ="Reference code sent successfully";

	public static final String EMPLOYEE_UPDATED_SUCCESSFULLY ="Employee updated successfully";


	public static final String INVALID_SESSION_ADD_REQUEST ="Invalid session add request";
	public static final String INVALID_SESSION_UPDATE_REQUEST ="Invalid session update request";

}
