package com.trs.fa.common.utils;

public interface SoftDeleteModel {
    boolean isSoftDelete();
}
