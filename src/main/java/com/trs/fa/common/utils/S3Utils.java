package com.trs.fa.common.utils;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import com.amazonaws.util.IOUtils;
import com.trs.fa.common.constant.MessageConstants;
import com.trs.fa.common.decorator.FileUploadTemplate;
import com.trs.fa.common.exception.InvalidRequestException;
import com.trs.fa.common.model.AdminConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Slf4j
public class S3Utils {

    public static String storeFileInAwsS3(AdminConfiguration configuration, String fileName, final byte[] fileByteArray,CannedAccessControlList cannedAccessControlList) {
        String uploadedFileName = fileName.replaceAll("\\s+", "_");
        if(StringUtils.isEmpty(configuration.getEndPoint()) || StringUtils.isEmpty(configuration.getBucketName())
                || StringUtils.isEmpty(configuration.getFileUploadAccessKey()) || StringUtils.isEmpty(configuration.getFileUploadSecretKey())
                || StringUtils.isEmpty(configuration.getFileUrlTemplate())){
            throw new InvalidRequestException(MessageConstants.FILE_UPLOAD_CONFIGURATION_NOT_SET_YET);
        }

        AmazonS3 amazonS3 = getAmazoneS3(configuration);

        InputStream stream = new ByteArrayInputStream(fileByteArray);
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength(fileByteArray.length);
        metadata.setCacheControl("public, max-age=31536000");
        amazonS3.putObject(new PutObjectRequest(configuration.getBucketName(), uploadedFileName, stream, metadata)
                .withCannedAcl(CannedAccessControlList.PublicRead));

        FileUploadTemplate fileUploadTemplate = new FileUploadTemplate();
        fileUploadTemplate.setEndPoint(configuration.getEndPoint());
        fileUploadTemplate.setBucketName(configuration.getBucketName());
        fileUploadTemplate.setFileUrl(uploadedFileName);

        //TODO using this due to SSL issue
        return new TemplateParser<FileUploadTemplate>().compileTemplate(configuration.getFileUrlTemplate(),fileUploadTemplate);
    }

    public static void deleteFileFromS3(AdminConfiguration configuration, String fileUrl) {

        if(StringUtils.isEmpty(fileUrl)){
            return;
        }

        AmazonS3 amazonS3 = getAmazoneS3(configuration);

        String fileName = FilenameUtils.getName(fileUrl);

        log.info("Deleted file : {}",fileName);

        amazonS3.deleteObject(new DeleteObjectRequest(configuration.getBucketName(),fileName));
    }
    private static AmazonS3 getAmazoneS3(AdminConfiguration configurations){
        String region = configurations.getRegion();
        if(StringUtils.isEmpty(region)){
            region = Regions.US_WEST_2.getName();
        }

        BasicAWSCredentials credentials = new BasicAWSCredentials(configurations.getFileUploadAccessKey(), configurations.getFileUploadSecretKey());
        return AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion(region)
                .build();
    }

    public static String generateRandomName() {
        return new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
    }

    public static String getExtension(String file){
        return Optional.ofNullable(file)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(file.lastIndexOf(".") + 1)).orElse("");
    }

    public static byte[] getImageFromS3(AdminConfiguration configuration,String key){
        byte[] content = null;
        AWSCredentials credentials = new BasicAWSCredentials(configuration.getFileUploadAccessKey(),
                configuration.getFileUploadSecretKey());
        AmazonS3Client conn = new AmazonS3Client(credentials);

        final S3Object s3Object = conn.getObject(configuration.getBucketName(), key);
        final S3ObjectInputStream stream = s3Object.getObjectContent();
        try {
            content = IOUtils.toByteArray(stream);
            s3Object.close();
        } catch(IOException ex) {
            log.info("IO Error Message= " + ex.getMessage());
        }
        return content;
    }

}
