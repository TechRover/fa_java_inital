package com.trs.fa.common.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public enum MenuType {

    SMALL("Small"),
    MEDIUM("Medium"),
    LARGE("Large");

    String name;

    MenuType(String name) {
        this.name = name;
    }

    public static List<Map<String,String>> toList(){
        return Arrays.stream(values()).map(MenuType::toMap).collect(Collectors.toList());
    }

    Map<String,String> toMap(){
        Map<String,String> map = new HashMap<>();
        map.put("name",this.name);
        map.put("value",this.toString());
        return map;
    }
}
