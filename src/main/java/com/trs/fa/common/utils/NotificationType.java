package com.trs.fa.common.utils;

public enum NotificationType {
    EMAIL,
    TEXT,
    BOTH,
    NONE,
}
