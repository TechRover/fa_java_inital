package com.trs.fa.common.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public enum CronScheduleType {

    DAILY("Daily"),
    WEEKLY("Weekly"),
    MONTHLY("Monthly");

    String name;

    CronScheduleType(String name) {
        this.name = name;
    }

    public static List<Map<String,String>> toList(){
        return Arrays.stream(values()).map(CronScheduleType::toMap).collect(Collectors.toList());
    }

    Map<String,String> toMap(){
        Map<String,String> map = new HashMap<>();
        map.put("name",name);
        map.put("value",this.toString());
        return map;
    }
}
