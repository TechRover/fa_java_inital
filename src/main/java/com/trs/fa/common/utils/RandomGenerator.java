package com.trs.fa.common.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class RandomGenerator {

    public static Date getRandomDate(int startYear,int endYear) {

        GregorianCalendar gc = new GregorianCalendar();

        int year = getRandomNumber(startYear, endYear);

        gc.set(Calendar.YEAR, year);

        int dayOfYear = getRandomNumber(1, gc.getActualMaximum(Calendar.DAY_OF_YEAR));

        gc.set(Calendar.DAY_OF_YEAR, dayOfYear);

        return gc.getTime();
    }

    public static int getRandomNumber(int start, int end) {
        return start + (int)Math.round(Math.random() * (end - start));
    }

    public static <T> T getUniqueRandomElement(int currentIndex, List<T> users){
        if(users.size()<=1){
            return users.get(currentIndex);
        }
        int randomNum = RandomGenerator.getRandomNumber(0, users.size()-1);
        if(currentIndex==randomNum){
            getUniqueRandomElement(currentIndex,users);
        }
        return users.get(randomNum);
    }
}
