package com.trs.fa.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.Date;

@Slf4j
public class MongoQueryUtils {
    public static Criteria durationQuery(TimeDuration timeDuration,Criteria criteria,String startTimeField,String endTimeField){
        return durationQuery(timeDuration,criteria,startTimeField,endTimeField,new Date());
    }
    public static Criteria durationQuery(TimeDuration timeDuration,Criteria criteria,String startTimeField,String endTimeField,int loseDuration){
        return durationQuery(timeDuration,criteria,startTimeField,endTimeField,new Date(),loseDuration);
    }

    public static Criteria durationQuery(TimeDuration timeDuration,Criteria criteria,String startTimeField,String endTimeField,Date date,int loseDuration){
        switch (timeDuration){
            case CURRENT:
                criteria = criteria.and(startTimeField).lte(date);
                criteria = criteria.and(endTimeField).gte(date);
                return criteria;
            case CURRENT_FUTURE:
                criteria = criteria.and(endTimeField).gte(date);
                return criteria;
            case PAST:
                return criteria.and(endTimeField).lte(date);
            case FUTURE:
                return criteria.and(startTimeField).gte(date);
            default:
                return criteria;
        }
    }
    public static Criteria durationQuery(TimeDuration timeDuration,Criteria criteria,String startTimeField,String endTimeField,Date date){
        return durationQuery(timeDuration,criteria, startTimeField,endTimeField,date,0);
    }
    public static Criteria durationQuery(TimeDuration timeDuration,Criteria criteria,String compareField,Date date){
        switch (timeDuration){
            case PAST:
                return criteria.and(compareField).lte(date);
            case FUTURE:
                return criteria.and(compareField).gte(date);
            default:
                return criteria;
        }
    }

    public static Criteria[] overlapCheck(String startTimeField,String endTimeField,Date startTime,Date endTime){
        Criteria startFieldOverlapCheck = checkBetweenFields(new Criteria(),startTime,startTimeField,endTimeField);
        Criteria endFieldOverlapCheck = checkBetweenFields(new Criteria(),endTime,startTimeField,endTimeField);
        Criteria startTimeOverlapCheck= checkBetweenDateRange(new Criteria(),startTime,endTime,startTimeField);
        Criteria endTimeOverlapCheck= checkBetweenDateRange(new Criteria(),startTime,endTime,endTimeField);
        return new Criteria[]{startFieldOverlapCheck,endFieldOverlapCheck,startTimeOverlapCheck,endTimeOverlapCheck};
    }



    private static Criteria checkBetweenFields(Criteria criteria, Date date, String startTimeField, String endTimeField){
        criteria = criteria.and(startTimeField).lte(date);
        criteria = criteria.and(endTimeField).gte(date);
        return criteria;
    }

    private static Criteria checkBetweenDateRange(Criteria criteria, Date startTime,Date endTime, String field){
        criteria = criteria.and(field).lte(endTime).gte(startTime);
            return criteria;
    }



    public static Criteria dateRangeCheck(Criteria criteria, Date startDate,Date endDate,String fieldName){
        if(startDate != null && endDate != null){
            criteria = criteria.andOperator(
                    Criteria.where(fieldName).gte(startDate),
                    Criteria.where(fieldName).lte(endDate)
            );
        }else if(startDate != null){
            criteria = criteria.and(fieldName).gte(startDate);
        }else if(endDate != null){
            criteria = criteria.and(fieldName).lte(endDate);
        }
        return criteria;
    }

    public static Criteria onDayCheck(Criteria criteria, Date day,String timezone,String fieldName){
        Date startDate = getStartTime(day,timezone);
        Date endDate = getEndTime(day,timezone);
        return dateRangeCheck(criteria,startDate,endDate,fieldName);
    }


    private static Date getStartTime(Date date,String timezone){
        DateTime dateTime = new DateTime(date);
        dateTime = dateTime.withZone(TimezoneHelper.getZoneId(timezone));
        dateTime = dateTime.withMinuteOfHour(0).withHourOfDay(0).withSecondOfMinute(0);
        return dateTime.toDate();
    }

    private static Date getEndTime(Date date,String timezone){
        DateTime dateTime = new DateTime(date);
        dateTime = dateTime.withZone(TimezoneHelper.getZoneId(timezone));
        dateTime = dateTime.withMinuteOfHour(59).withHourOfDay(23).withSecondOfMinute(59);
        return dateTime.toDate();
    }
}
