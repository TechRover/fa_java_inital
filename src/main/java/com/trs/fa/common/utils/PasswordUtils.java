package com.trs.fa.common.utils;

import org.mindrot.jbcrypt.BCrypt;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class PasswordUtils {

	public static String encryptPassword(String password) {

		// gensalt's log_rounds parameter determines the complexity
		// the work factor is 2**log_rounds, and the default is 10

		return BCrypt.hashpw(password, BCrypt.gensalt(12));
	}

	public static boolean isPasswordAuthenticated(String userProvided, String passwordInDatabase, PasswordEncryptionType passwordType) throws NoSuchAlgorithmException {
		// Check that an unencrypted password matches one that has
		// previously been hashed

		// If Password bi s null
		if(passwordInDatabase == null){
			return false;
		}

		// Check the encryption type
		if(passwordType == null || passwordType.equals(PasswordEncryptionType.BCRYPT)) {
			return BCrypt.checkpw(userProvided, passwordInDatabase);
		}else {
			return encryptPasswordToMD5(userProvided).toLowerCase().equals(passwordInDatabase.toLowerCase());
		}
	}
	private static String encryptPasswordToMD5(String password) throws NoSuchAlgorithmException {
		MessageDigest messageDigest = MessageDigest.getInstance("MD5");
		messageDigest.update(password.getBytes());
		return DatatypeConverter.printHexBinary(messageDigest.digest());
	}

}
