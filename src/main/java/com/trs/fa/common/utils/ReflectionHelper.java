package com.trs.fa.common.utils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ReflectionHelper {

    public static List<String> getDecalredFields(Class c){
        List<String> fields = new ArrayList<>();
        Field[] fields1 = c.getFields();
        for(Field f : fields1){
            fields.add(f.getName());
        }
        return fields;
    }
}
