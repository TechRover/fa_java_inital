package com.trs.fa.common.utils;

import com.trs.fa.common.services.ConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


@Service
public class GeneralHelper {

    @Autowired
    ConfigurationService configService;

    public PageRequest getPagination(Integer page, Integer size) {
        if (page == null) {
            page = 0;
        }
        if (size == null) {
            size = 10;
        }
        return PageRequest.of(page, size);
    }



}
