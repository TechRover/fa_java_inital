package com.trs.fa.common.utils;

import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;

import java.util.List;

public class MongoProjectionHelper {

    public static ProjectionOperation fromListString(List<String> fields){
        ProjectionOperation projectionOperation = Aggregation.project();
        for(String field : fields){
            projectionOperation = projectionOperation.andInclude(field);
        }
        return projectionOperation;
    }
}
