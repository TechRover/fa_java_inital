package com.trs.fa.common.utils;

public class AuthenticationException extends Exception {

    /**
	 * Default serial version UID
	 */
	private static final long serialVersionUID = 1L;

	public AuthenticationException(String message) {
        super(message);
    }
}
