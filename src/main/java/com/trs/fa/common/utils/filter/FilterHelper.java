package com.trs.fa.common.utils.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.Collection;
import java.util.Map;

@Slf4j
public class FilterHelper {

    public static Criteria getCriteria(Criteria startCriteria, Object filterObject){
        Map<String,FilterField> fields = AnnotationHelper.getFieldsWithAnnotation(filterObject.getClass(),FilterField.class);
        CustomCriteria customCriteria = new CustomCriteria();

        for(String key : fields.keySet()){
            FilterField filterField = fields.get(key);
            String fieldName = filterField.value().equals("") ? key : filterField.value();
            Object object = AnnotationHelper.callGetter(filterObject,key);

            if(object != null){
                customCriteria.addCriteria(updateCriteria(new Criteria(),fieldName,filterField.type(),object));
            }
        }
        return customCriteria.getCriteria(startCriteria);
    }

    static Criteria updateCriteria(Criteria criteria, String fieldName, FilterType filterType, Object value){
        switch (filterType){
            case IS:
                return criteria.and(fieldName).is(value);
            case IN:
                return criteria.and(fieldName).in((Collection<?>) value);
            case GT:
                return criteria.and(fieldName).gt(value);
            case GTE:
                return criteria.and(fieldName).gte(value);
            case LT:
                return criteria.and(fieldName).lt(value);
            case LTE:
                return criteria.and(fieldName).lte(value);
            case NOT:
                return criteria.and(fieldName).ne(value);
            case NOT_IN:
                return criteria.and(fieldName).nin((Collection<?>) value);
        }
        return criteria;
    }
}
