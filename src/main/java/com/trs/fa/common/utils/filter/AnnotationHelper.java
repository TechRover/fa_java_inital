package com.trs.fa.common.utils.filter;

import lombok.extern.slf4j.Slf4j;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class AnnotationHelper {

    public static <A extends Annotation> Map<String,A > getFieldsWithAnnotation(Class cl, Class<A> annotation){
        List<Field> fields = Arrays.asList(cl.getDeclaredFields());
        Map<String,A> annotationList = new HashMap<>();
        for (Field field : fields) {
            A annotation1 = field.getAnnotation(annotation);
            if(annotation1 == null){
                continue;
            }
            annotationList.put(field.getName(),annotation1);
        }
        return annotationList;
    }

    public static Object callGetter(Object obj, String fieldName){
        PropertyDescriptor pd;
        try {
            pd = new PropertyDescriptor(fieldName, obj.getClass());
            return pd.getReadMethod().invoke(obj);
        } catch (IntrospectionException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ignore) {}
        return null;
    }
}
