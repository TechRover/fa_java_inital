package com.trs.fa.common.utils;

import com.trs.fa.common.model.RestAPIs;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;



@Slf4j
public class Utils {

    /**
     * This method creates a list of method with its authorization
     * "getUserDetail" [USER, ADMIN, TECH_ADMIN]
     *
     * @param className
     * @return
     */
    @SuppressWarnings("rawtypes")
    public static List<RestAPIs> getAllMethodNames(Class className) {
        Method[] allMethods = className.getDeclaredMethods();
        List<RestAPIs> apis = new ArrayList<RestAPIs>();
        for (Method method : allMethods) {

            if (Modifier.isPublic(method.getModifiers())) {
                Access a = method.getAnnotation(Access.class);
                RequestMapping rm = method.getAnnotation(RequestMapping.class);
                log.info("Name : {} , Access : {} ",rm.name(),a);
                if(a != null){
                    List<String> authList = new ArrayList<>(Arrays.asList(a.levels()))
                            .stream()
                            .map(Enum::toString)
                            .collect(Collectors.toList());

                    RestAPIs api = new RestAPIs();
                    api.setName(rm.name());
                    api.setApiGroup(a.apiGroup());
                    api.setRoleGroup(a.roleGroup());
                    api.setRoles(authList);
                    apis.add(api);

                }

            }
        }
        return apis;
    }
}
