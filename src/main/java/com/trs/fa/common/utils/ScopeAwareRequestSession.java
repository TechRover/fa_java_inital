package com.trs.fa.common.utils;

import com.trs.fa.common.model.JWTUser;
import com.trs.fa.common.model.RequestSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;

import java.util.ArrayList;

@Service
public class ScopeAwareRequestSession {

    @Autowired
    ApplicationContext applicationContext;


    public JWTUser getJwtUser() {
        if (RequestContextHolder.getRequestAttributes() == null) {
            return new JWTUser("INTERNAL_SYSTEM",null,new ArrayList<>());
        }
        RequestSession requestSession = applicationContext.getBean(RequestSession.class);
        return requestSession.getJwtUser();
    }


    public String getTimezone(){
        if (RequestContextHolder.getRequestAttributes() == null) {
            return "UTC";
        }
        RequestSession requestSession = applicationContext.getBean(RequestSession.class);
        return requestSession.getTimezone();
    }

}
