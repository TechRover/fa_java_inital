package com.trs.fa.common.utils.filter;

import java.util.function.Function;

public enum CompareObjectTypes {

    BOOLEAN(Boolean::parseBoolean);


    private Function<String,Object> processor;

    CompareObjectTypes(Function<String, Object> processor) {
        this.setProcessor(processor);
    }

    public Function<String, Object> getProcessor() {
        return processor;
    }

    public void setProcessor(Function<String, Object> processor) {
        this.processor = processor;
    }
}
