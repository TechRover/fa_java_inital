package com.trs.fa.common.utils;

public class RolesGroup {

    public static final Role[] USER = {Role.USER};
    public static final Role[] ADMIN = {Role.ADMIN};
    public static final Role[] DOCTOR_OFFICE = {Role.DOCTOR,Role.NURSE,Role.ADMIN,Role.OWNER};
}
