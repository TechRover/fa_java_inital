package com.trs.fa.common.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public enum MemberStatus {
    PENDING("Pending"),
    APPROVED("Approved"),
    REJECTED("Rejected");

    String name;

    MemberStatus(String name) {
        this.name = name;
    }

    public static List<Map<String,String>> toList(){
        return Arrays.stream(values()).map(MemberStatus::toMap).collect(Collectors.toList());
    }

    Map<String,String> toMap(){
        Map<String,String> map = new HashMap<>();
        map.put("name",this.name);
        map.put("value",this.toString());
        return map;
    }
}
