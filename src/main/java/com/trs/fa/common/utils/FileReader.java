package com.trs.fa.common.utils;

import com.google.common.io.Resources;
import lombok.extern.slf4j.Slf4j;

import java.net.URL;
import java.nio.charset.StandardCharsets;

@Slf4j
public class FileReader {

    public static String loadFile(String fileName) {

        try{
            URL url = Resources.getResource(fileName);
            return Resources.toString(url, StandardCharsets.UTF_8);
        }catch (Exception e){
            return "";
        }
    }


}
