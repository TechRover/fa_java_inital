package com.trs.fa.common.utils;

public class RoleConstants {

    public static final String VERIFICATION_TOKEN = "VERIFICATION_TOKEN";
    public static final String CHANGE_PASSWORD_TOKEN = "CHANGE_PASSWORD_TOKEN";
}
