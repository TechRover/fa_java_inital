package com.trs.fa.common.utils.filter;

public enum  FilterType {
    IS, IN, NOT_IN, NOT, GTE, LTE, GT, LT;
}
