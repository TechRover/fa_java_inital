package com.trs.fa.common.utils.filter;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Search {


    String searchPattern() default ".*%s.*";
    String regexOption() default "i";

    Class<?> searchInClass() default Object.class;

    String[] searchInFields() default  {};
}
