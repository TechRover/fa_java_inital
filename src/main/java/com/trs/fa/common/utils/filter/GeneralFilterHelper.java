package com.trs.fa.common.utils.filter;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Component;

@Component
public class GeneralFilterHelper {

    @Autowired
    ModelMapper modelMapper;

    public Criteria getCriteria(Criteria startCriteria, Object filter){
        CustomCriteria customCriteria = new CustomCriteria();
        customCriteria.addCriteria(SearchFilterHelper.getCriteria(new Criteria(),filter));
        customCriteria.addCriteria(FilterHelper.getCriteria(new Criteria(),filter));

        return customCriteria.getCriteria(startCriteria);
    }
    public Criteria getCriteria(Criteria startCriteria, Object filter,Class<?> c){
        return getCriteria(startCriteria,modelMapper.map(filter,c));
    }
}
