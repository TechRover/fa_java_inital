package com.trs.fa.common.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
public enum AuthUserSortBy {

    FIRST_NAME("firstName","First name"),
    LAST_NAME("lastName","Last name"),
    BIRTH_YEAR("birthYear","Birth year"),
    EMAIL("email","Email"),
    PHONE("phone","Phone"),
    ADDRESS("addressLine1","Address"),
    CITY("city","City"),
    STATE("state","State"),
    COUNTRY("country","Country");

    @JsonIgnore
    private String value;

    @JsonIgnore
    private String name;

    AuthUserSortBy(String value, String name) {
        this.value = value;
        this.name = name;
    }

    public static List<Map<String,String>> toList(){
        return Arrays.stream(values()).map(AuthUserSortBy::toMap).collect(Collectors.toList());
    }

    Map<String,String> toMap(){
        Map<String,String> map = new HashMap<>();
        map.put("name",name);
        map.put("value",this.toString());
        return map;
    }

}
