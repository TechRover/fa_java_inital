package com.trs.fa.common.utils;

public class ApiGroupConstants {

    public static final String APPOINTMENT_USER = "APPOINTMENT_USER";
    public static final String APPOINTMENT_MANAGE = "APPOINTMENT_MANAGE";
    public static final String APPOINTMENT = "APPOINTMENT";
    public static final String DOCUMENT = "DOCUMENT";
    public static final String WELLNESS = "WELLNESS";
    public static final String MEDICATION = "MEDICATION";
    public static final String MANAGE_USER = "MANAGE_USER";
    public static final String MANAGE_PLAN = "MANAGE_PLAN";



}
