package com.trs.fa.common.utils.excel;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;


@Data
@NoArgsConstructor
public class ImportedData {
    List<String> headers;

    Map<String,List<String>> data;
}
