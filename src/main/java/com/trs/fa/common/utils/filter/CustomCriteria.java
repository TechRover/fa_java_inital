package com.trs.fa.common.utils.filter;

import org.springframework.data.mongodb.core.query.Criteria;

import java.util.ArrayList;
import java.util.List;

public class CustomCriteria {

    List<Criteria> criteria;

    public CustomCriteria() {
        criteria = new ArrayList<>();
    }

    public void addCriteria(Criteria newCriteria){
        criteria.add(newCriteria);
    }

    public Criteria getCriteria(Criteria startCriteria){
        return CriteriaHelper.getAndCriteriaFromList(criteria,startCriteria);
    }
}
