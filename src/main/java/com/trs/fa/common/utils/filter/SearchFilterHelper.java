package com.trs.fa.common.utils.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.query.Criteria;

import java.lang.reflect.Field;
import java.util.*;

@Slf4j
public class SearchFilterHelper {


    public static Criteria getCriteria(Criteria startCriteria, Object filterObject){
        // 1. Get Condition Search Fields
        Map<String,List<String>> conditionSearchField = getConditionSearchFields(filterObject);

        Map<String,Search> fields =  AnnotationHelper.getFieldsWithAnnotation(filterObject.getClass(),Search.class);
        // Make A Global And Criteria
        List<Criteria> andCriteria = new ArrayList<>();

        for (String fieldName: fields.keySet()) {
            String searchValue = (String) AnnotationHelper.callGetter(filterObject,fieldName);
            if(searchValue == null){
                continue;
            }
            // Processing Each Search Element
            List<String> searchFields = getSearchField(fields.get(fieldName));
            if(conditionSearchField.containsKey(fieldName)){
                searchFields.addAll(conditionSearchField.get(fieldName));
            }

            //log.info("Search Fields -> {}", searchFields);
            //log.info("Search Values -> {}", searchValue);
            List<Criteria> searchCriteria = getSearchCriteria(fields.get(fieldName),searchFields,searchValue);
            andCriteria.add(CriteriaHelper.getOrCriteriaFromList(searchCriteria));
        }
        return CriteriaHelper.getAndCriteriaFromList(andCriteria,startCriteria);
    }


    public static Map<String,List<String>> getConditionSearchFields(Object filterObject){
        Map<String,AndSearchIf> conditionSearchFields = AnnotationHelper.getFieldsWithAnnotation(filterObject.getClass(),AndSearchIf.class);
        Map<String,List<String>> conditionSearches = new HashMap<>();
        for(String field : conditionSearchFields.keySet()){
            AndSearchIf searchIf = conditionSearchFields.get(field);
            Object currentValue = AnnotationHelper.callGetter(filterObject,field);
            if(currentValue == null){
                // If value is not set the continue to the next loop ...
                continue;
            }
            Object checkValue = searchIf.compareType().getProcessor().apply(searchIf.compareObject());
            if(currentValue.equals(checkValue)){
                List<String> searchFields = getSearchField(searchIf);
                addToMap(conditionSearches,searchFields,searchIf.searchFilter());
            }
        }
        return conditionSearches;
    }


    private static void addToMap(Map<String,List<String>> data,List<String> values,String key){
        if(!data.containsKey(key)){
            // Initialize if not added ...
            data.put(key,new ArrayList<>());
        }
        data.get(key).addAll(values);
    }




    private static List<Criteria> getSearchCriteria(Search search,List<String> fieldNames,String value){
        List<Criteria> orCriteria = new ArrayList<>();
        for(String fieldName : fieldNames){
            // Loop Over and create all criteria
            Criteria criteria = Criteria.where(fieldName).regex(
                    String.format(search.searchPattern(),value),
                    search.regexOption());
            orCriteria.add(criteria);
        }
        return orCriteria;
    }





    public static List<String> getSearchField(Search search) {
        List<String> searchFields =  new ArrayList<>();
        if(Objects.nonNull(search.searchInClass())){
            Field[] declaredFields = search.searchInClass().getDeclaredFields();
            for (Field field: declaredFields) {
                updateListFromSearchField(field,searchFields);
            }
            searchFields.addAll(Arrays.asList(search.searchInFields()));
        }
        return searchFields;
    }

    public static List<String> getSearchField(AndSearchIf search) {
        List<String> searchFields =  new ArrayList<>();
        if(Objects.nonNull(search.searchInClass())){
            Field[] declaredFields = search.searchInClass().getDeclaredFields();
            for (Field field: declaredFields) {
                updateListFromSearchField(field,searchFields);
            }
            searchFields.addAll(Arrays.asList(search.searchInFields()));
        }
        return searchFields;
    }



    private static void updateListFromSearchField(Field field,List<String> fields){
        SearchField searchField = field.getAnnotation(SearchField.class);
        if(!Objects.nonNull(searchField)){
            //log.info("Field Value Was not added {}",field.getName());
            return;
        }
        if(searchField.value().equals("")){
            //log.info("Using Search From Field Name {}",field.getName());
            fields.add(field.getName());
        }else{
            //log.info("Using Search From Annotation {}",field.getName());
            fields.add(searchField.value());
        }

    }
}
