package com.trs.fa.common.utils;

import lombok.Getter;

@Getter
public enum TimeDurationType {

    //DAY("Day",1),
    MONTH("Month",2,"MMM-YYYY"),
    QUARTER("Quarter",4,""),
    YEAR("Year",10,"YYYY");

    String name;

    int maxYear;

    String dateFormat;

    TimeDurationType(String name, int maxYear, String dateFormat) {
        this.name = name;
        this.maxYear = maxYear;
        this.dateFormat = dateFormat;
    }

}
