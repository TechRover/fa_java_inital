package com.trs.fa.common.utils;

import com.trs.fa.common.exception.EncryptionException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

public class EncryptionUtils {

	private static SecretKeySpec secretKey;
	private static byte[] key;

	/**
	 * Set the payment encryption key
	 * 
	 * @param myKey
	 */
	private static void setKey(String myKey) {
		MessageDigest sha = null;
		try {
			key = myKey.getBytes("UTF-8");
			sha = MessageDigest.getInstance("SHA-1");
			key = sha.digest(key);
			key = Arrays.copyOf(key, 16);
			secretKey = new SecretKeySpec(key, "AES");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Encrypt using the secret j
	 * 
	 * @param strToEncrypt
	 * @param secret
	 * @return
	 * @throws EncryptionException
	 */
	public static String encrypt(String strToEncrypt, String secret) throws EncryptionException {
		try {
			setKey(secret);
			Cipher cipher = Cipher.getInstance(EncryptionConstant.CYPHER_ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
			return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8)));
		} catch (Exception e) {
			throw new EncryptionException("Error while encrypting: ", e.getCause());
		}

	}

	/**
	 * Decrypt using the secret
	 * 
	 * @param strToDecrypt
	 * @param secret
	 * @return
	 */
	public static String decrypt(String strToDecrypt, String secret) throws EncryptionException {
		try {
			setKey(secret);
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
		} catch (Exception e) {
			throw new EncryptionException("Error while decrypting: ", e);
		}
	}


}
