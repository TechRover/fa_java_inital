package com.trs.fa.common.utils;

/**
 * Persona/Roles for Authorization
 * @author TRS
 *
 */
public enum Role {
	USER, ADMIN, TECH_ADMIN, ANONYMOUS,VOLUNTEER,VENDOR,SYSTEM,DOCTOR,NURSE,ADMIN_STAFF,OWNER,VERIFICATION_TOKEN,CHANGE_PASSWORD_TOKEN
}
