package com.trs.fa.common.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;


@Service
public class PaginationHelper {

    @Value("${trs.defaults.pagingLimit}")
    String pageLimit;


    public Pageable getPagination(Integer page){
        if(page == null){
            return PageRequest.of(1,Integer.valueOf(pageLimit));
         }else{
            return PageRequest.of(page,Integer.valueOf(pageLimit));
        }
    }
}
