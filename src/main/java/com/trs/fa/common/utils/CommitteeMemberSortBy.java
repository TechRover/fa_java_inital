package com.trs.fa.common.utils;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
public enum CommitteeMemberSortBy {

    FIRST_NAME("firstName","First name",true,false),
    LAST_NAME("lastName","Last name",true,false),
    EMAIL("email","Email",true,false),
    PHONE("phone","Phone",true,false),

    CHAPTER("name","Date",false,true),

    START_YEAR("startYear","Start year",false,false),
    END_YEAR("endYear","End year",false,false),
    STATUS("activeMember","Status",false,false),
    DESIGNATION("designation","Designation",false,false);

    @JsonIgnore
    private String value;

    @JsonIgnore
    private String name;

    @JsonIgnore
    private boolean allowInAuth;

    @JsonIgnore
    private boolean allowInChapter;


    CommitteeMemberSortBy(String value, String name, boolean allowInAuth, boolean allowInChapter) {
        this.value = value;
        this.name = name;
        this.allowInAuth = allowInAuth;
        this.allowInChapter = allowInChapter;
    }

    public static List<Map<String,String>> toList(){
        return Arrays.stream(values()).map(CommitteeMemberSortBy::toMap).collect(Collectors.toList());
    }

    Map<String,String> toMap(){
        Map<String,String> map = new HashMap<>();
        map.put("name",name);
        map.put("value",this.toString());
        return map;
    }

}
