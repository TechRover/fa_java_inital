package com.trs.fa.common.utils.filter;

import org.springframework.data.mongodb.core.query.Criteria;

import java.util.ArrayList;
import java.util.List;

public class CriteriaHelper {


    // OR Operator
    public static Criteria getOrCriteriaFromList(List<Criteria> criteriaList,Criteria start){
        if(criteriaList.size() == 0){
            return start;
        }
        return start.orOperator(
                new ArrayList<>(criteriaList).toArray(new Criteria[criteriaList.size()])
        );
    }

    public static Criteria getOrCriteriaFromList(List<Criteria> criteriaList) {
        return getOrCriteriaFromList(criteriaList,new Criteria());
    }

    // And Operator...

    public static Criteria getAndCriteriaFromList(List<Criteria> criteriaList,Criteria start){
        if(criteriaList.size() == 0){
            return start;
        }
        return start.andOperator(
                new ArrayList<>(criteriaList).toArray(new Criteria[criteriaList.size()])
        );
    }
    public static Criteria getAndCriteriaFromList(List<Criteria> criteriaList) {
        return getAndCriteriaFromList(criteriaList,new Criteria());
    }
}
