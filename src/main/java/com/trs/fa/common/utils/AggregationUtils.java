package com.trs.fa.common.utils;

import org.springframework.data.mongodb.core.aggregation.*;
import org.springframework.data.mongodb.core.query.Criteria;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;


/**
 * 
 * @author TechRover Solutions
 *
 */
public class AggregationUtils {

	
//	public static com.trs.fa.notification.utils.CustomAggregationOperation unWindAggrBuilder(String unwindField, boolean preserveNull){
//		com.trs.fa.notification.utils.CustomAggregationOperation aggregationOperation = new com.trs.fa.notification.utils.CustomAggregationOperation(
//		        new Document("$unwind",  new Document("path", unwindField).append("preserveNullAndEmptyArrays", preserveNull))
//		    );
//		return aggregationOperation;
//	}
//
//
//	public static com.trs.fa.notification.utils.CustomAggregationOperation sortIntervalDateAggregationBuilder(String sortField){
//		com.trs.fa.notification.utils.CustomAggregationOperation aggregationOperation = new com.trs.fa.notification.utils.CustomAggregationOperation(
//		        new Document("$orderBy", new Document(sortField,-1))
//		    );
//		return aggregationOperation;
//	}
//
//
//	public static com.trs.fa.notification.utils.CustomAggregationOperation lookupBuilder(String fromField, String locaField, String foreignField, String asField) {
//		return new CustomAggregationOperation(
//		        new Document(
//		            "$lookup",
//		            new Document("from", fromField)
//		                .append("localField", locaField)
//		                .append("foreignField", foreignField)
//		                .append("as", asField)
//		        )
//		    );
//	}

	public static List<AggregationOperation> getCountAndTotalAggregationByDate(Class collection, TimeDurationType timeDurationType, Criteria matchFilter, String dateField){

		List<AggregationOperation> operations = new ArrayList<>();

		if(dateField!=null){
			matchFilter = matchFilter.and(dateField).ne(null);
		}
		operations.add(match(matchFilter));

		/*if(timeDurationType == null || timeDurationType.equals(TimeDurationType.DAY)) {
			operations.add(project(collection)
					.and(DateOperators.DateToString.dateOf("$"+dateField).toString("%Y-%m-%d")).as("date")
			);
		}else*/ if(timeDurationType == null || timeDurationType.equals(TimeDurationType.MONTH)) {
			operations.add(project(collection)
					.and(DateOperators.DateToString.dateOf("$"+dateField).toString("%Y-%m")).as("field")
			);
		}else if (timeDurationType.equals(TimeDurationType.YEAR)){
			operations.add(project(collection)
					.and(DateOperators.DateToString.dateOf("$"+dateField).toString("%Y")).as("field")
			);
		} else {
			operations.add(project(collection)
					.and(DateOperators.DateToString.dateOf("$"+dateField).toString("%Y")).as("field")
					.and(ConditionalOperators.switchCases(creatQuarterCondition(dateField))).as("quarter")
			);
		}

		if(timeDurationType!=null && timeDurationType.equals(TimeDurationType.QUARTER)){
			operations.add(project(collection)
				.and(StringOperators.Concat.valueOf("$quarter").concat("-").concatValueOf("$field")).as("field")
				.and(StringOperators.Concat.valueOf("$field").concat("-").concatValueOf("$quarter")).as("dateSort")
			);
		}else {
			operations.add(project(collection)
				.and(StringOperators.Concat.valueOf("$field")).as("field")
				.and(StringOperators.Concat.valueOf("$field")).as("dateSort")
			);
		}

        /*Document document = new Document();

        if(timeDurationType == null || timeDurationType.equals(TimeDurationType.DAY)) {
            document = new Document("date",
                    new Document("$dateToString",
                            new Document("date", "$" + dateField)
                                    .append("format", "%Y-%m-%d")))
                    .append(filedName, 1);
        }else if(timeDurationType.equals(TimeDurationType.MONTH)) {
            document = new Document("date",
                    new Document("$dateToString",
                            new Document("date", "$" + dateField)
                                    .append("format", "%Y-%m")))
                    .append(filedName, 1);
        }else {
            document = new Document("date",
                    new Document("$dateToString",
                            new Document("date", "$" + dateField)
                                    .append("format", "%Y")))
                    .append(filedName, 1);
        }

        if(extraProjection != null){
            document.putAll(extraProjection);
        }

        aggregationOperations.add(
                new CustomAggregationOperation(new Document("$project",document)));
        aggregationOperations.add(
                new CustomAggregationOperation(
                        new Document("$group",
                                new Document("_id","$date")
                                        .append(outputName,
                                                new Document(opration,"$"+filedName)))
                )
        );

        // Format the result in proper manner
        aggregationOperations.add(
                new CustomAggregationOperation(new Document("$project",
                        new Document("date","$_id")
                                .append(outputName,1))
                )
        );*/

		return operations;
	}
	private static List<ConditionalOperators.Switch.CaseOperator> creatQuarterCondition(String dateField){
		List<ConditionalOperators.Switch.CaseOperator> caseOperators = new ArrayList<>();

		caseOperators.add(ConditionalOperators.Switch.CaseOperator.when(
				ComparisonOperators.Lte.valueOf(DateOperators.Month.monthOf("$"+dateField)).lessThanEqualToValue(3)
		).then("Q1"));
		caseOperators.add(ConditionalOperators.Switch.CaseOperator.when(
				ComparisonOperators.Lte.valueOf(DateOperators.Month.monthOf("$"+dateField)).lessThanEqualToValue(6)
		).then("Q2"));
		caseOperators.add(ConditionalOperators.Switch.CaseOperator.when(
				ComparisonOperators.Lte.valueOf(DateOperators.Month.monthOf("$"+dateField)).lessThanEqualToValue(9)
		).then("Q3"));
		caseOperators.add(ConditionalOperators.Switch.CaseOperator.when(
				ComparisonOperators.Lte.valueOf(DateOperators.Month.monthOf("$"+dateField)).lessThanEqualToValue(12)
		).then("Q4"));
		return caseOperators;
	}
}
