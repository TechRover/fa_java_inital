package com.trs.fa.common.utils;

import com.trs.fa.common.model.PathTrail;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * This Annotation Use with AspectJ To make sure only if user that created the document (like event) can edit the event
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface UserAccessCheck {

    public Class<? extends MongoRepository<? extends PathTrail,String>> value(); // Specify the document class here...
}
