package com.trs.fa.common.utils;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface EnsureIdCheck {
    public Class<? extends MongoRepository<? extends  SoftDeleteModel,String>> value(); // Specify the document class here...

}
