package com.trs.fa.common.utils;


import com.samskivert.mustache.Mustache;


public class TemplateParser<T> {

    private Mustache.Compiler mustache = Mustache.compiler().defaultValue("").escapeHTML(false);


    public String compileTemplate(String template, T data){
        return mustache.compile(template).execute(data);
    }

}
