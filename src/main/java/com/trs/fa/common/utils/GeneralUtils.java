package com.trs.fa.common.utils;

import java.nio.charset.StandardCharsets;
import java.util.Random;

public class GeneralUtils {

    public static String generateRandomString(int size){
        byte[] array = new byte[size]; // length is bounded by 7
        new Random().nextBytes(array);

        return new String(array, StandardCharsets.UTF_8);

    }

    public static String generateString(int length) {
        String token_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890abcdefghijklmnopqrstuvxyz";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < length) {
            int index = (int) (rnd.nextFloat() * token_chars.length());
            salt.append(token_chars.charAt(index));
        }
        return salt.toString();

    }
}
