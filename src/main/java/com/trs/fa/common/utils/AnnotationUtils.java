package com.trs.fa.common.utils;

import com.trs.fa.common.exception.NotFoundException;

import java.lang.reflect.Parameter;

public class AnnotationUtils {


    public static int getParameterByAnnotationPosition(Parameter[] params, Class annotation) throws NotFoundException {
        for(int i = 0;i <  params.length;i++){
            Parameter p = params[i];
            if(p.getAnnotation(annotation) != null){
                return i;
            }
        }
        throw new NotFoundException();
    }

    public static Parameter getParameterByAnnotation(Parameter[] params, Class annotation) throws NotFoundException {
        for(int i = 0;i <  params.length;i++){
            Parameter p = params[i];
            if(p.getAnnotation(annotation) != null){
                return p;
            }
        }
        throw new NotFoundException();
    }

}
