package com.trs.fa.common.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public enum SchedulerWeekDay {
    SUN("Sun"),
    MON("Mon"),
    TUE("Tue"),
    WED("Wed"),
    THU("Thu"),
    FRI("Fri"),
    SAT("Sat");

    String name;
    String fullName;

    SchedulerWeekDay(String name) {
        this.name = name;
    }

    public static List<Map<String,String>> toList(){
        return Arrays.stream(values()).map(SchedulerWeekDay::toMap).collect(Collectors.toList());
    }

    Map<String,String> toMap(){
        Map<String,String> map = new HashMap<>();
        map.put("name",name);
        map.put("value",this.toString());
        return map;
    }
}
