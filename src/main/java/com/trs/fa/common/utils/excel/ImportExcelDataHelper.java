package com.trs.fa.common.utils.excel;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class ImportExcelDataHelper {

    public static ImportedData getDataFromExcel(InputStream fileStream) throws IOException {
        ImportedData importedData = new ImportedData();
        Workbook workbook = new XSSFWorkbook(fileStream);
        // Get The Active Sheet for import work around for not putting the index static
        Sheet sheet = workbook.getSheetAt(workbook.getActiveSheetIndex());
        Map<String,List<String>> data = new HashMap<>();
        Iterator<Row> rows = sheet.iterator();
        // Setup Header Row
        Row row = rows.next();
        List<String> headers = getHeaders(row);
        headers.forEach(header ->{
            data.put(header,new ArrayList<>());
        });
        importedData.setHeaders(headers);


        while (rows.hasNext()){
            int i = 0;
            Row currentRow = rows.next();
            for (Cell currentCell : currentRow) {
                String cellValue = null;
                if(currentCell.getCellType().equals(CellType.STRING)){
                    cellValue = currentCell.getStringCellValue();
                } else if (currentCell.getCellType().equals(CellType.NUMERIC)) {
                    cellValue = String.valueOf(currentCell.getNumericCellValue());
                }
                String headerCell = headers.get(i++);
                data.get(headerCell).add(cellValue);
            }
        }
        importedData.setData(data);
        return importedData;

    }


    private static List<String> getHeaders(Row headerRow){
        List<String> data = new ArrayList<>();
        Iterator<Cell> cells = headerRow.iterator();
        while (cells.hasNext()) {
            Cell currentCell = cells.next();
            String cellValue = currentCell.getStringCellValue();
            data.add(cellValue);
        }
        return data;
    }
}
