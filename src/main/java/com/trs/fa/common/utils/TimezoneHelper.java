package com.trs.fa.common.utils;

import org.joda.time.DateTimeZone;

import java.util.TimeZone;

public class TimezoneHelper {

    public static TimeZone fromSortId(String shortId){
        return TimeZone.getTimeZone(shortId);
    }
    public static DateTimeZone getZoneId(String timezone){
        return DateTimeZone.forTimeZone(TimeZone.getTimeZone(timezone));
    }
}
