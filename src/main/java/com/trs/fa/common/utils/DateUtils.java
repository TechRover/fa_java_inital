package com.trs.fa.common.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.Date;

@Slf4j
public class DateUtils {

    public static boolean isDateBetween(Date startDate, Date endDate, Date checkDate){
        log.info("Checking Btiween -> {} , {} , vs {}  ANS : {}",startDate,endDate,checkDate,startDate.getTime() < checkDate.getTime() && endDate.getTime() > checkDate.getTime()
        );
        return startDate.getTime() < checkDate.getTime() && endDate.getTime() > checkDate.getTime();
    }

    public static boolean isOverlapping(Date startDate1, Date startDate2, Date endDate1,Date endDate2){
        log.info("===============================================");
        return isDateBetween(startDate1,endDate1,startDate2) ||
                isDateBetween(startDate1,endDate1,endDate2) ||
                isDateBetween(startDate2,endDate2,startDate1) ||
                isDateBetween(startDate2,endDate2,endDate1);


    }

}
