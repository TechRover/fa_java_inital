package com.trs.fa.common.connector;

import com.google.gson.Gson;
import com.trs.fa.common.decorator.AuthUser;
import com.trs.fa.common.decorator.DataResponse;
import com.trs.fa.common.decorator.GeneralUser;
import com.trs.fa.common.decorator.ListResponse;
import com.trs.fa.common.exception.GeneralServiceException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class AuthConnector extends MicroserviceConnector{


    public List<GeneralUser> getUsers(List<String> userIds){
        return getUsers(userIds,null);
    }
    public List<GeneralUser> getUsers(List<String> userIds,String search) {
        Map<String, Object> filter = new HashMap<>();
        Map<String, Object> sort = new HashMap<>();
        filter.put("userIds",userIds.stream().filter(userId-> !StringUtils.isEmpty(userId)));
        if(search != null){
            filter.put("search",search);
        }
        sort.put("fieldName", "firstName");
        sort.put("orderBy", 1);
        Map<String, Object> data = getFilterAndSortRequest(filter,sort);
        ParameterizedTypeReference<ListResponse<GeneralUser>> typeRef = new ParameterizedTypeReference<ListResponse<GeneralUser>>() {};
        return postRequest("user/findAll",data,typeRef).getData();
    }

    public GeneralUser findById(String id){
        List<GeneralUser> users = getUsers(Collections.singletonList(id));
        if(users.size() != 0){
            return users.get(0);
        }
        return null;
    }


    public String saveUser(AuthUser authUser){
        ParameterizedTypeReference<DataResponse<String>> typeRef = new ParameterizedTypeReference<DataResponse<String>>() {};
        DataResponse<String> dataResponse = postRequest("user/saveUser",authUser,typeRef);
        log.info("Save Success full -> {}",dataResponse);
//        throw new RuntimeException();
        return dataResponse.getData();
    }
    public String addUser(AuthUser authUser) throws GeneralServiceException {
        ParameterizedTypeReference<DataResponse<String>> typeRef = new ParameterizedTypeReference<DataResponse<String>>() {};
        DataResponse<String> dataResponse = postRequest("user/addUser",authUser,typeRef);
        if(!dataResponse.getStatus().getCode().equals(HttpStatus.OK)){
            throw new GeneralServiceException(dataResponse.getStatus());
        }
        return dataResponse.getData();
    }
    public Object updateUser(AuthUser authUser,String userId){
        ParameterizedTypeReference<DataResponse<Object>> typeRef = new ParameterizedTypeReference<DataResponse<Object>>() {};
        String dataResponse = postRequest("user/update/"+userId,authUser,String.class);
        log.info("Response From Update API -> {}",dataResponse);
        return new Gson().fromJson(dataResponse,typeRef.getType());
    }
    public List<String> saveUserBulk(List<AuthUser> authUser){
        ParameterizedTypeReference<ListResponse<String>> typeRef = new ParameterizedTypeReference<ListResponse<String>>() {};
        ListResponse<String> dataResponse = postRequest("user/saveBulkUser",authUser,typeRef);
        return dataResponse.getData();
    }

    @Override
    public String getBaseUrl() {
        return super.getBaseUrl()+"/auth/";
    }
}
