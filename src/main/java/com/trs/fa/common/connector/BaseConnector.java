package com.trs.fa.common.connector;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@Slf4j
public abstract class BaseConnector {


    public <T> T postRequest(String url, Object requestData, ParameterizedTypeReference<T> responseObject){

        HttpEntity<Object> httpEntity = new HttpEntity<>(requestData,getHeaders());
        log.info("Data From Server -> {}",httpEntity);
        ResponseEntity<T> responseEntity = getRestTemplate().exchange(getBaseUrl()+url, HttpMethod.POST,httpEntity,responseObject);
        return responseEntity.getBody();
    }

    public <T> T postRequest(String url, Object requestData, Class<T> responseObject){

        HttpEntity<Object> httpEntity = new HttpEntity<>(requestData,getHeaders());
        log.info("Data From Server -> {}",httpEntity);
        ResponseEntity<T> responseEntity = getRestTemplate().exchange(getBaseUrl()+url, HttpMethod.POST,httpEntity,responseObject);
        return responseEntity.getBody();
    }

    public <T> T getRequest(String url, Class<T> responseObject){

        HttpEntity<Object> httpEntity = new HttpEntity<>(getHeaders());
        ResponseEntity<T> responseEntity = getRestTemplate().exchange(getBaseUrl()+url, HttpMethod.GET,httpEntity,responseObject);
        return responseEntity.getBody();
    }


    public abstract RestTemplate getRestTemplate();

    public abstract String getBaseUrl();


    public abstract HttpHeaders getHeaders();
}
