package com.trs.fa.common.connector;

import com.trs.fa.common.model.JWTUser;
import com.trs.fa.common.services.ConfigurationService;
import com.trs.fa.common.utils.CustomHTTPHeaders;
import com.trs.fa.common.utils.JwtTokenUtil;
import com.trs.fa.common.utils.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public abstract class MicroserviceConnector extends BaseConnector{

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ConfigurationService configurationService;

    @Autowired
    JwtTokenUtil jwtTokenUtil;

    @Override
    public RestTemplate getRestTemplate() {
         return restTemplate;
    }

    @Override
    public String getBaseUrl() {
        return configurationService.getConfiguration().getBaseUrl();
    }


    private String generateSystemToken(){
        // TODO Make it dynamic
        return jwtTokenUtil.generateToken(new JWTUser("COMMON",null,Collections.singletonList(Role.SYSTEM.toString())));
    }


    @Override
    public HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.set(CustomHTTPHeaders.TOKEN.toString(), generateSystemToken());
        headers.set("accept","application/json");
        headers.set("Content-Type","application/json");
        return headers;
    }



    protected Map<String,Object> getFilterAndSortRequest(Object filter,Object sort){
        Map<String,Object> map = new HashMap<>();
        map.put("filter",filter);
        map.put("sortRequest",sort);
        return map;
    }
}
