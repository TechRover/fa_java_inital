package com.trs.fa.common.listener;

import com.trs.fa.common.controller.*;
import com.trs.fa.common.model.RestAPIs;
import com.trs.fa.common.repository.RestAPIRepository;
import com.trs.fa.common.utils.Utils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class ApplicationStartUpEventListener {

	@Value("${trs.defaults.pagingLimit}")
	String pageLimit;

	boolean skip =  true;

	@Autowired
	RestAPIRepository restAPIRepository;

//	@Autowired
//	CommunityAdminConfigRepository configRepository;

	@EventListener()
	@Async
	public void onApplicationEvent(ContextRefreshedEvent event) {
		log.debug("Landed in here");

//		List<CommunityAdminConfiguration> configurations = configRepository.findAll();
//		if(configurations.size() > 0){
//			log.debug("Module Technical configurations exists");
//		}else {
//			CommunityAdminConfiguration configuration = new CommunityAdminConfiguration();
//			configuration.setEmailNotification(true);
//			configuration.setTextNotification(false);
//			configuration.setDefaultPageSize(Integer.valueOf(pageLimit));
//			configuration.setCreatedBy("SYSTEM");
//			configuration.setCreated(new Date());
//			configuration.setUpdatedBy("SYSTEM");
//			configuration.setUpdated(new Date());
//			configuration.setCurrency("$");
//			configuration.setTaxInPercent(5);
//
//			configRepository.insert(configuration);
//
//			log.debug("Automatically create the module technical configurations");
//		}


		// On Application Start up , create the list of authorized services for authorized data
		if(!skip){
			saveIfNotExits(Utils.getAllMethodNames(ConfigController.class));
			saveIfNotExits(Utils.getAllMethodNames(TranslationsController.class));
			saveIfNotExits(Utils.getAllMethodNames(FileUploadController.class));
			saveIfNotExits(Utils.getAllMethodNames(CommonUtilsController.class));
			saveIfNotExits(Utils.getAllMethodNames(CountryController.class));
		}
	}

	private void saveIfNotExits(List<RestAPIs> apis){
		apis.forEach(api->{
			if(!restAPIRepository.existsByName(api.getName())){
				log.info("Added API : {}",api.getName());
				restAPIRepository.insert(api);
			}
		});

	}
}
