package com.trs.fa.common.controller;

import com.trs.fa.common.decorator.DataResponse;
import com.trs.fa.common.decorator.ListResponse;
import com.trs.fa.common.model.AdminConfiguration;
import com.trs.fa.common.services.ConfigurationService;
import com.trs.fa.common.services.ResponseManager;
import com.trs.fa.common.utils.Access;
import com.trs.fa.common.utils.Role;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigController {

    @Autowired
    ConfigurationService configService;

    @Autowired
    ResponseManager responseManager;

    @Autowired
    ModelMapper modelMapper;

    @RequestMapping(name="configurationUpdate",value = "/configuration/update", method = RequestMethod.POST)
    @Access(levels = {Role.ADMIN})
    public DataResponse<AdminConfiguration> configurationUpdate(@RequestBody AdminConfiguration configurationRequest){
        DataResponse<AdminConfiguration> dataResponse = new DataResponse<>();
        try {
            dataResponse.setData(configService.updateConfiguration(configurationRequest));
            dataResponse.setStatus(responseManager.getOkResponse());
        } catch (Exception e) {
            e.printStackTrace();
            dataResponse.setStatus(responseManager.getInternalServerErrorResponse());
        }
        return dataResponse;
    }

    @RequestMapping(name="getReferralOptions",value = "/configuration/referralOption", method = RequestMethod.GET)
    @Access(levels = {Role.ANONYMOUS})
    public ListResponse<String> getReferralOptions(){
        ListResponse<String> listResponse = new ListResponse<>();
        try {
            listResponse.setData(configService.getReferralOptions());
            listResponse.setStatus(responseManager.getOkResponse());
        } catch (Exception e) {
            e.printStackTrace();
            listResponse.setStatus(responseManager.getInternalServerErrorResponse());
        }
        return listResponse;
    }

    @RequestMapping(name="getTermsAndCondition",value = "/configuration/termsAndCondition", method = RequestMethod.GET)
    @Access(levels = {Role.ANONYMOUS})
    public DataResponse<String> getTermsAndCondition(){
        DataResponse<String> listResponse = new DataResponse<>();
        try {
            listResponse.setData(configService.getTermsAndCondition());
            listResponse.setStatus(responseManager.getOkResponse());
        } catch (Exception e) {
            e.printStackTrace();
            listResponse.setStatus(responseManager.getInternalServerErrorResponse());
        }
        return listResponse;
    }
}
