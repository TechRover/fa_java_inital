package com.trs.fa.common.controller;

import com.trs.fa.common.services.TranslationsService;
import com.trs.fa.common.utils.Access;
import com.trs.fa.common.utils.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/translation")
public class TranslationsController {

    @Autowired
    TranslationsService translationsService;

    @RequestMapping(name="getTranslation",value = "/{name}/{lang}",method = RequestMethod.GET)
    @Access(levels = Role.ANONYMOUS)
    public Map<String,String> getTranslations(@PathVariable String name, @PathVariable String lang){
        return translationsService.findByNameAndLang(name,lang);
    }


}
