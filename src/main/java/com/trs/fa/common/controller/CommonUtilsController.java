package com.trs.fa.common.controller;

import com.trs.fa.common.decorator.ListResponse;
import com.trs.fa.common.decorator.Response;
import com.trs.fa.common.model.AdminConfiguration;
import com.trs.fa.common.model.YearListConfiguration;
import com.trs.fa.common.services.ConfigurationService;
import com.trs.fa.common.utils.Access;
import com.trs.fa.common.utils.Role;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/common/utils")
@Slf4j
public class CommonUtilsController {


    @Autowired
    ConfigurationService configurationService;



    @RequestMapping(name="getYearsRange",value={"/year/{range}"},method = RequestMethod.GET)
    @Access(levels =  {Role.ANONYMOUS})
    public ListResponse<Pair<Integer,Integer>> getYearsRange(@PathVariable Integer range){
        AdminConfiguration adminConfiguration = configurationService.getConfiguration();
        YearListConfiguration yearConfig = adminConfiguration.getYearListConfiguration();
        List<Pair<Integer,Integer>> pairList = new ArrayList<>();
        log.info("Year CConfig :  {}",yearConfig);
        for(int i= yearConfig.getStartYear(); i < yearConfig.getEndYear();){
           int startYear = i;
           int endyear = Math.min(yearConfig.getEndYear(),i+range);
            Pair<Integer,Integer> pair = Pair.of(startYear,endyear);
            pairList.add(pair);
           i=endyear;
        }
        return new ListResponse<>(pairList,Response.getOkResponse());

    }



}
