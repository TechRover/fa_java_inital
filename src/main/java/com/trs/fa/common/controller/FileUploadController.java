package com.trs.fa.common.controller;

import com.trs.fa.common.decorator.DataResponse;
import com.trs.fa.common.decorator.DeleteMediaRequest;
import com.trs.fa.common.decorator.FileUrlWithThumbnail;
import com.trs.fa.common.decorator.Response;
import com.trs.fa.common.model.RequestSession;
import com.trs.fa.common.services.FileUploadService;
import com.trs.fa.common.utils.Access;
import com.trs.fa.common.utils.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import static com.trs.fa.common.constant.MessageConstants.FILE_DELETED_SUCCESSFULLY;
import static com.trs.fa.common.constant.MessageConstants.FILE_UPLOADED_SUCCESSFULLY;
import static com.trs.fa.common.constant.ResponseConstant.OK;

@RestController
public class FileUploadController {

    @Autowired
    FileUploadService fileUploadService;

    @Autowired
    RequestSession requestSession;

    @RequestMapping(name = "uploadFile", value = "/file/upload", method = RequestMethod.POST, consumes ={ "multipart/form-data" })
    @Access(levels = { Role.USER,Role.ADMIN})
    public DataResponse<FileUrlWithThumbnail> uploadFile(@RequestParam(value="file") MultipartFile file) {
        DataResponse<FileUrlWithThumbnail> dataResponse=new DataResponse<>();

        dataResponse.setData(fileUploadService.getMediaUrl(file,requestSession.getJwtUser().getId()));
        dataResponse.setStatus(new Response(HttpStatus.OK, OK, FILE_UPLOADED_SUCCESSFULLY));

        return dataResponse;
    }

    @RequestMapping(name = "deleteFile", value = "/file/delete", method = RequestMethod.POST)
    @Access(levels = { Role.USER,Role.ADMIN})
    public DataResponse<Object> deleteFile(@RequestBody DeleteMediaRequest deleteMediaRequest) {
        DataResponse<Object> dataResponse=new DataResponse<>();

        String userId = requestSession.getJwtUser().getId();
        if(requestSession.getJwtUser().hasRole(Role.ADMIN.toString())){
            userId = null;
        }
        fileUploadService.deleteMediaFile(deleteMediaRequest,userId);
        dataResponse.setStatus(new Response(HttpStatus.OK, OK, FILE_DELETED_SUCCESSFULLY));

        return dataResponse;
    }
}
