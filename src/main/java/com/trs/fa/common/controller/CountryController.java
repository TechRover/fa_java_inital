package com.trs.fa.common.controller;

import com.trs.fa.common.decorator.ListResponse;
import com.trs.fa.common.model.Country;
import com.trs.fa.common.services.CountryService;
import com.trs.fa.common.services.ResponseManager;
import com.trs.fa.common.utils.Access;
import com.trs.fa.common.utils.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("country")
public class CountryController {

    @Autowired
    CountryService countryService;

    @Autowired
    ResponseManager responseManager;

    @RequestMapping(name="getAllCountry",value = "/getAll", method = RequestMethod.GET)
    @Access(levels = {Role.ANONYMOUS})
    public ListResponse<Country> getAllCountry(){
        ListResponse<Country> listResponse = new ListResponse<>();
        try {
            listResponse.setData(countryService.getAllCountry());
            listResponse.setStatus(responseManager.getOkResponse());
        } catch (Exception e) {
            e.printStackTrace();
            listResponse.setStatus(responseManager.getInternalServerErrorResponse());
        }
        return listResponse;
    }
}
