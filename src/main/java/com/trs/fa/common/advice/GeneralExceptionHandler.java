package com.trs.fa.common.advice;


import com.trs.fa.common.decorator.DataResponse;
import com.trs.fa.common.decorator.Response;
import com.trs.fa.common.exception.AlreadyExistsException;
import com.trs.fa.common.exception.AuthException;
import com.trs.fa.common.exception.InvalidRequestException;
import com.trs.fa.common.exception.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GeneralExceptionHandler {

    @ExceptionHandler(AlreadyExistsException.class)
    public ResponseEntity<DataResponse<Object>> alreadyExits(HttpServletRequest req, AlreadyExistsException ex) {
        return new ResponseEntity<>(new DataResponse<>(null, new Response(HttpStatus.CONFLICT, "BAD_REQUEST", ex.getMessage())), HttpStatus.OK);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<DataResponse<Object>> resourceNotFound(HttpServletRequest req, NotFoundException ex) {
        return new ResponseEntity<>(new DataResponse<>(null, new Response(HttpStatus.NOT_FOUND, "BAD_REQUEST", ex.getMessage())), HttpStatus.OK);
    }

    @ExceptionHandler(InvalidRequestException.class)
    public ResponseEntity<DataResponse<Object>> invalidRequest(HttpServletRequest req, InvalidRequestException ex) {
        return new ResponseEntity<>(new DataResponse<>(null, new Response(HttpStatus.NOT_FOUND, "BAD_REQUEST", ex.getMessage())), HttpStatus.OK);
    }

    @ExceptionHandler(AuthException.class)
    public ResponseEntity<DataResponse<Object>> authorizationError(HttpServletRequest req, AuthException ex) {
        return new ResponseEntity<>(new DataResponse<>(null, new Response(HttpStatus.UNAUTHORIZED, "BAD_REQUEST", ex.getMessage())), HttpStatus.OK);
    }

}
