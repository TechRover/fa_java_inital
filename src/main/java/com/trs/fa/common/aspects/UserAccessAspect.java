package com.trs.fa.common.aspects;


import com.trs.fa.common.constant.MessageConstants;
import com.trs.fa.common.constant.ResponseConstant;
import com.trs.fa.common.decorator.DataResponse;
import com.trs.fa.common.decorator.ListResponse;
import com.trs.fa.common.decorator.PageResponse;
import com.trs.fa.common.model.PathTrail;
import com.trs.fa.common.model.RequestSession;
import com.trs.fa.common.services.ResponseManager;
import com.trs.fa.common.utils.AnnotationUtils;
import com.trs.fa.common.utils.UserAccessCheck;
import com.trs.fa.common.utils.UserAccessCheckId;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.http.HttpStatus;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Optional;

import static com.trs.fa.common.constant.MessageConstants.UNAUTHORIZED_ACCESS;
import static com.trs.fa.common.utils.Role.ADMIN;

@Aspect
@Configuration
public class UserAccessAspect {

    @Autowired
    RequestSession requestSession;

    @Autowired
    ResponseManager responseManager;

    @Autowired
    ApplicationContext applicationContext;

    @Around("@annotation(com.trs.fa.common.utils.UserAccessCheck)")
    Object userAspectCheck(ProceedingJoinPoint joinPoint) throws Throwable {
        try{
            if(requestSession.getJwtUser().hasRole(ADMIN.toString())){
                // if admin then no need to process further
                return joinPoint.proceed(joinPoint.getArgs());
            }
            MethodSignature ms = (MethodSignature) joinPoint.getSignature();
            Method m = ms.getMethod();
            int portion  = AnnotationUtils.getParameterByAnnotationPosition(m.getParameters(), UserAccessCheckId.class);
            Parameter parameter  = AnnotationUtils.getParameterByAnnotation(m.getParameters(),UserAccessCheckId.class);
            UserAccessCheckId checkId = parameter.getAnnotation(UserAccessCheckId.class);
            String id = null;
            // TODO YM Refactor this
            if(!checkId.value().equals("")){
                Object object = joinPoint.getArgs()[portion];
                Field f =  object.getClass().getDeclaredField(checkId.value());
                Method[] methods = object.getClass().getDeclaredMethods();
                for(Method method: methods){
                    if(method.getName().toUpperCase().contains(checkId.value().toUpperCase())){
                        id = (String) method.invoke(object);
                        break;
                    }
                }
                if(id == null){
                    throw new  Exception(String.format(MessageConstants.GETTER_NOT_FOUND,checkId.value()));
                }
            }else{

                if(joinPoint.getArgs()[portion] instanceof Optional){
                    Optional optional = (Optional) joinPoint.getArgs()[portion];
                    if(!optional.isPresent()){
                        return joinPoint.proceed(joinPoint.getArgs());
                    }
                    id = (String) optional.get();
                }else{
                    id = (String) joinPoint.getArgs()[portion];
                }
            }
            UserAccessCheck userAccessCheck = m.getAnnotation(UserAccessCheck.class);
            MongoRepository<? extends PathTrail,String> mongoRepository = applicationContext.getBean(userAccessCheck.value());
            if(id == null){
                return joinPoint.proceed(joinPoint.getArgs());
            }
            Optional<? extends PathTrail> pathTrail = mongoRepository.findById(id);

            if(pathTrail.isPresent()) {
                if (!requestSession.getJwtUser().getId().equals(pathTrail.get().getCreatedBy())) {
                    if (m.getReturnType().equals(DataResponse.class)) {
                        return new DataResponse<>(null,responseManager.getResponse(HttpStatus.UNAUTHORIZED, ResponseConstant.ERROR, UNAUTHORIZED_ACCESS));
                    } else if(m.getReturnType().equals(ListResponse.class)) {
                        return new ListResponse<>(null, responseManager.getResponse(HttpStatus.UNAUTHORIZED, ResponseConstant.ERROR, UNAUTHORIZED_ACCESS));
                    } else {
                        return new PageResponse<>(null, responseManager.getResponse(HttpStatus.UNAUTHORIZED, ResponseConstant.ERROR, UNAUTHORIZED_ACCESS));
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            // If the Parameter with UserAccessCheckId not found Then Continue Without error
            return joinPoint.proceed(joinPoint.getArgs());
        }
        return joinPoint.proceed(joinPoint.getArgs());

    }

}
