package com.trs.fa.common.aspects;


import com.trs.fa.common.constant.ResponseConstant;
import com.trs.fa.common.decorator.DataResponse;
import com.trs.fa.common.decorator.ListResponse;
import com.trs.fa.common.decorator.PageResponse;
import com.trs.fa.common.decorator.Response;
import com.trs.fa.common.utils.AnnotationUtils;
import com.trs.fa.common.utils.EnsureId;
import com.trs.fa.common.utils.EnsureIdCheck;
import com.trs.fa.common.utils.SoftDeleteModel;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.http.HttpStatus;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Optional;

@Aspect
@Configuration
public class EnsureAspect {

    @Autowired
    ApplicationContext applicationContext;

    @Around("@annotation(com.trs.fa.common.utils.EnsureIdCheck)")
    Object userAspectCheck(ProceedingJoinPoint joinPoint) throws Throwable {
        try{
            MethodSignature ms = (MethodSignature) joinPoint.getSignature();
            Method m = ms.getMethod();
            int portion  = AnnotationUtils.getParameterByAnnotationPosition(m.getParameters(), EnsureId.class);
            Parameter parameter  = AnnotationUtils.getParameterByAnnotation(m.getParameters(),EnsureId.class);
            EnsureId checkId = parameter.getAnnotation(EnsureId.class);
            String id = null;
            // TODO YM Refactor this
            if(!checkId.value().equals("")){
                Object object = joinPoint.getArgs()[portion];
                Field f =  object.getClass().getDeclaredField(checkId.value());
                Method[] methods = object.getClass().getDeclaredMethods();
                for(Method method: methods){
                    if(method.getName().toUpperCase().contains(checkId.value().toUpperCase())){
                        id = (String) method.invoke(object);
                        break;
                    }
                }
                if(id == null){
                    throw new  Exception();
                }
            }else{

                if(joinPoint.getArgs()[portion] instanceof Optional){
                    Optional optional = (Optional) joinPoint.getArgs()[portion];
                    if(!optional.isPresent()){
                        return joinPoint.proceed(joinPoint.getArgs());
                    }
                    id = (String) optional.get();
                }else{
                    id = (String) joinPoint.getArgs()[portion];
                }
            }
            EnsureIdCheck ensureId = m.getAnnotation(EnsureIdCheck.class);
            MongoRepository<? extends SoftDeleteModel,String> mongoRepository = applicationContext.getBean(ensureId.value());
            if(id == null){
                return joinPoint.proceed(joinPoint.getArgs());
            }
            Optional<? extends SoftDeleteModel> pathTrail = mongoRepository.findById(id);

            if(!pathTrail.isPresent() || pathTrail.get().isSoftDelete()) {
                if (m.getReturnType().equals(DataResponse.class)) {
                    return new DataResponse<>(null, new Response(HttpStatus.NOT_FOUND, ResponseConstant.ERROR,checkId.message() ));
                } else if(m.getReturnType().equals(ListResponse.class)) {
                    return new ListResponse<>(null, new Response(HttpStatus.NOT_FOUND, ResponseConstant.ERROR, checkId.message()));
                } else if(m.getReturnType().equals(PageResponse.class)){
                    return new PageResponse<>(null, new Response(HttpStatus.NOT_FOUND, ResponseConstant.ERROR, checkId.message() ));
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            // If the Parmater with UserAccessCheckId not found Then Continue Without error
            return joinPoint.proceed(joinPoint.getArgs());
        }
        return joinPoint.proceed(joinPoint.getArgs());

    }

}
