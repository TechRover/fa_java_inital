package com.trs.fa.common.decorator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.trs.fa.common.utils.filter.FilterField;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SoftDeleteFilter {

    @FilterField
    @JsonIgnore
    boolean softDelete = false;
}
