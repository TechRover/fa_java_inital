package com.trs.fa.common.decorator;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class IdResponse {

    String id;

    public IdResponse(String id) {
        this.id = id;
    }
}
