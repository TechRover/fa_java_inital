package com.trs.fa.common.decorator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.data.domain.PageRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Pagination {

	Integer pageNumber;
	Integer pageLimit;


	@JsonIgnore
	public PageRequest getPageRequest(){
		return PageRequest.of(pageNumber,pageLimit);
	}
}
