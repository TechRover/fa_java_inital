package com.trs.fa.common.decorator;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ShareNotification {

    String type;

    GeneralUser sharedBy;


    public ShareNotification(String type, GeneralUser sharedBy) {
        this.type = type;
        this.sharedBy = sharedBy;
    }
}
