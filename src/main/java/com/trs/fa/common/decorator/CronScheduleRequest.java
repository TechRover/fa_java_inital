package com.trs.fa.common.decorator;

import com.trs.fa.common.utils.CronScheduleType;
import com.trs.fa.common.utils.SchedulerWeekDay;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CronScheduleRequest {

    CronScheduleType scheduleType;

    int repeatCount;

    // Only For Weekly
    List<SchedulerWeekDay> weekDays;

    // Only For Monthly
    Integer dayOfMonth;     // Day of the month that schedule should execute (if Monthly)
    // or
    CronMonthWeekDay cronMonthWeekDay;

}
