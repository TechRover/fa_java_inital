package com.trs.fa.common.decorator;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.List;

@JsonRootName( value = "status")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ResponseListData<T> {

    HttpStatus code;
    String status;
    String description;
    List<T> data;

    public static <T> ResponseListData<T> getOkResponse(){
        return new ResponseListData<T>(HttpStatus.OK,"OK","",null);
    }

    public static <T> ResponseListData<T> getError(){
        return new ResponseListData<T>(HttpStatus.BAD_REQUEST,"ERROR","",null);
    }

}

