package com.trs.fa.common.decorator;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import static com.trs.fa.common.constant.ResponseConstant.*;

@JsonRootName ( value = "status")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Response {

    HttpStatus code;
    String status;
    String description;

    public static Response getOkResponse(){
        return new Response(HttpStatus.OK, OK, OK_DESCRIPTION);
    }
    public static Response getOkResponse(String description){
        return new Response(HttpStatus.OK, OK, description);
    }

    public static Response getSuccessResponse() {
        return new Response(HttpStatus.OK, SUCCESS, OK_DESCRIPTION);
    }
    public static Response getSuccessResponse(String message) {
        return new Response(HttpStatus.OK, SUCCESS, message);
    }

    public static Response getCreatedResponse(String message){
        return new Response(HttpStatus.CREATED, SUCCESS, message);
    }

    public static Response getUpdatedResponse(String message){
        return new Response(HttpStatus.OK, UPDATED, message);
    }

    public static Response getDeletedResponse(String message){
        return new Response(HttpStatus.OK, DELETED, message);
    }


    public static Response getInvalidRequestResponse(){
        return new Response(HttpStatus.BAD_REQUEST, ERROR, INVALID_REQUEST_DESCRIPTION);
    }
    public static Response getInvalidRequestResponse(String message){
        return new Response(HttpStatus.BAD_REQUEST, ERROR,message);
    }

    public static Response getNotFoundResponse(){
        return new Response(HttpStatus.NOT_FOUND, ERROR, NOT_FOUND_DESCRIPTION);
    }
    public static Response getNotFoundResponse(String message){
        return new Response(HttpStatus.NOT_FOUND, ERROR, message);
    }

    public static Response getInternalServerErrorResponse() {
        return new Response(HttpStatus.INTERNAL_SERVER_ERROR, ERROR, HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
    }
    public static Response getInternalServerErrorResponse(String message) {
        return new Response(HttpStatus.INTERNAL_SERVER_ERROR, ERROR, message);
    }

    public static Response getErrorResponse(String message, HttpStatus statusCode) {
        return new Response(statusCode, ERROR, message);
    }

}
