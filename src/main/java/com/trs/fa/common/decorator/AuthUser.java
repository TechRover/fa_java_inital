package com.trs.fa.common.decorator;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class AuthUser {

    String firstName;

    String lastName;

    Date birthDate;

    String phone;

    String email;

    String address;

    String designation;

    String state;

    String city;

    String pincode;

    List<String> roles;

    String profileImageUrl;

    String countryCode;



}
