package com.trs.fa.common.decorator;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class ShareRequest {

    Set<String> sharedUsers;
    
}
