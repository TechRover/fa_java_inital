package com.trs.fa.common.decorator;

import lombok.*;
import org.springframework.data.domain.Sort;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class FilterSortRequest<FILTER,SORT> {
    FILTER filter;
    SortRequest<SORT> sort;
    Pagination page;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    @ToString
    public static class SortRequest<SORT> {
        SORT    sortBy;
        Sort.Direction orderBy;
    }

}
