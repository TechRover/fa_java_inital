package com.trs.fa.common.decorator;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class DeleteMediaRequest {

    String link;

    String thumbnailLink;
}
