package com.trs.fa.common.decorator;

import com.trs.fa.common.utils.SchedulerWeekDay;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class CronMonthWeekDay {

    int weekIndex; // first second ...

    SchedulerWeekDay weekDay;

}
