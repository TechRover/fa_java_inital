package com.trs.fa.common.decorator;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CountResponse {
    int count;

    public CountResponse(int count) {
        this.count = count;
    }
}
