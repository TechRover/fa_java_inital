package com.trs.fa.common.decorator;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UpdateUserInfo extends  GeneralUser{

    String userId;
}
