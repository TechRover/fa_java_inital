package com.trs.fa.common.decorator;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FileUploadTemplate {

    String endPoint;

    String bucketName;

    String fileUrl;
}
