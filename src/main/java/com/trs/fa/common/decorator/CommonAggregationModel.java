package com.trs.fa.common.decorator;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CommonAggregationModel {

    String responseId;

    String referenceId;

    String referenceToUnset;
}
