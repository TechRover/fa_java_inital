package com.trs.fa.common.decorator;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.Date;
import java.util.Set;

@Data
@Getter
@Setter
@ToString
public class GeneralUser {

    String id;

    String firstName;

    String lastName;

    String email;

    String address;

    String state;

    Date birthDate;

    String city;

    String pincode;

    String phone;

    String designation;

    String countryCode;

    String profileImageUrl; // Profile Image Url

    String publicKey;

    Set<String> token;
}
