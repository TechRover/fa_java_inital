package com.trs.fa.common.decorator;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class DataResponse<T> {
    T data;
   Response status;
}
