package com.trs.fa.common.config;


import com.trs.fa.common.model.RequestSession;
import com.trs.fa.common.utils.NullAwareBeanUtilsBean;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.annotation.RequestScope;

@Configuration
public class GeneralBeans {

    @Bean
    ModelMapper getModelMapper(){
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        return modelMapper;
    }

    @Bean
    public RestTemplate getRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
//        restTemplate.setInterceptors(ImmutableList.of(new LoggingRequestInterceptor()));
        return restTemplate;
    }


    @Bean
    @RequestScope
    public RequestSession getRequestSession(){
        return new RequestSession();
    }

    @Bean
    public NullAwareBeanUtilsBean beanUtilsBean(){return new NullAwareBeanUtilsBean();}




}

