package com.trs.fa.common.exception;

public class PaymentFailException extends Exception{
    public PaymentFailException(String message){super(message);}
}
