package com.trs.fa.common.exception;

import com.trs.fa.common.decorator.Response;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class GeneralServiceException extends  Exception {

    Response response;

    public GeneralServiceException(Response response) {
        this.response = response;
    }
}
