package com.trs.fa.common.exception;

public class PaymentFailed extends  Exception {
    public PaymentFailed(String message) {
        super(message);
    }
}
