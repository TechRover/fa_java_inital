package com.trs.fa.common.exception;

public class MessageNotSentException extends Exception {
    public MessageNotSentException(String message){
        super(message);
    }
}
