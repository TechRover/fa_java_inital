package com.trs.fa.common.exception;

public class AuthException extends RuntimeException {

    public AuthException(String message) {
        super(message);
    }
}
