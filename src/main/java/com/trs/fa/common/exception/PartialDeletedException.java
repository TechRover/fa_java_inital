package com.trs.fa.common.exception;

public class PartialDeletedException extends Exception {

    public PartialDeletedException(String message) {
        super(message);
    }
}
