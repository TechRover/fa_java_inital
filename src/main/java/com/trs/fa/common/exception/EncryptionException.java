package com.trs.fa.common.exception;

public class EncryptionException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public EncryptionException(String message, Throwable t) {
		super(message, t);
	}
}
