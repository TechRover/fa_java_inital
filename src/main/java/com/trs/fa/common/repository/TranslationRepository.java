package com.trs.fa.common.repository;

import com.trs.fa.common.model.Translations;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface TranslationRepository  extends MongoRepository<Translations,String> {
    Optional<Translations> findByNameAndLang(String name,String lang);
}
