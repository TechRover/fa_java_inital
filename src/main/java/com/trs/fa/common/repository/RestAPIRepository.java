package com.trs.fa.common.repository;

import com.trs.fa.common.model.RestAPIs;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Community Repository
 * @author TRS
 *
 */
@Repository
public interface RestAPIRepository extends MongoRepository<RestAPIs, String> {
    List<RestAPIs> findByNameAndRolesIn(String name, List<String> roles);

    boolean existsByName(String name);

}
