package com.trs.fa.common.repository;

import com.trs.fa.common.model.AdminConfiguration;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

/**
 * Authentication Configurations available to Tech Admin and Admin
 * @author TRS
 *
 */
@Repository
public interface AdminConfigurationRepository extends MongoRepository<AdminConfiguration, String> {


}
