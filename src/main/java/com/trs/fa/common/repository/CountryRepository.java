package com.trs.fa.common.repository;

import com.trs.fa.common.model.Country;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CountryRepository extends MongoRepository<Country,String> {

    List<Country> findBySoftDeleteIsFalse();

}
