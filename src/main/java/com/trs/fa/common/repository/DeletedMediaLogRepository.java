package com.trs.fa.common.repository;

import com.trs.fa.common.model.DeletedMediaLog;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface DeletedMediaLogRepository extends MongoRepository<DeletedMediaLog,String> {

}
