package com.trs.fa.auth;

import com.trs.fa.CloudStorageFileUpload;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
@Slf4j
public class CloudStorageTest {

    @Test
    void uploadFile() throws IOException {
        log.info(new CloudStorageFileUpload().uploadFile());
    }

}
