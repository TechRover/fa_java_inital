package com.trs.fa.auth.data;

import com.trs.fa.common.utils.CustomAggregationOperation;
import com.trs.fa.common.utils.FileReader;
import lombok.extern.slf4j.Slf4j;
import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Slf4j
public class AggregationTest {

    @Test
    void generateDocumentFromJson(){

        try {
            String json = FileReader.loadFile("aggregation/getMyReferrals.json");
            JSONObject jsonObject = new JSONObject(json);

            String project_1 = CustomAggregationOperation.getJson(jsonObject,"get_active_status_and_year",Object.class);

            log.info("Json data : {}",project_1);
            log.info("Operation : {}",new CustomAggregationOperation(Document.parse(project_1)));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
