FROM openjdk:8-jdk-alpine
RUN adduser fm_user  --disabled-password
ADD target/java_init-0.0.1-SNAPSHOT.jar  app.jar
EXPOSE 6500
ENV JAVA_OPTS="-Dspring.profiles.active=dev -XX:+UseSerialGC -XX:+UnlockExperimentalVMOptions -XX:MinRAMPercentage=60.0 -XX:MaxRAMPercentage=90.0 -XX:+HeapDumpOnOutOfMemoryError"
USER fm_user
ENTRYPOINT [ "sh", "-c", "java  $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar /app.jar" ]

